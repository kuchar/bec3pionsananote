\section{Coulomb effect}
\label{sec:Coulomb}

The double ratio of the correlation functions is defined in~(\ref{eqt:DR}). As it may be observed in Fig.~\ref{fig:DR_pions} there is an enhancement in all bins of VELO track multiplicity in the region of low particle momenta difference, where the Bose-Einstein interference enhancement is expected. On the other hand, in the region of higher $Q$ the distribution is in good approximation flat and close to 1. Fig.~\ref{fig:DR_pions_U} shows the DR distributions for unlike-sign pion pairs, where the observed deviation from the straight line results from the imperfect simulation of the $\pi^{+} \pi^{-}$ resonant structure which occurs for the unlike-sign pion sample (see also Fig.~\ref{fig:CF_unlike_DATA} and Fig.~\ref{fig:CF_unlike_MC} in Sect.~\ref{sec:Systematics}).


\subsection{Description of the Coulomb correction}

The final state interactions (FSI) involving both electromagnetic (Coulomb) and strong forces, are present in the Bose-Einstein correlation region and may potentially affect the distributions of analysed observables. Strong final state interactions are present in both charged and neutral hadron systems, while the Coulomb interactions exert an influence only on the charged hadron systems.

At small $Q$, the Coulomb repulsion between two identically charged hadrons is presumed to alter the correlation function $C_{2}(Q)$ by decreasing the signal enhancement. For that reason the density distributions are corrected for this effect applying the so-called Gamov penetration factor~\cite{Coulomb} $G_{2}(Q)$ per track pair with a weight $1 / G(Q)$. The effective density distribution is defined as follows:

\begin{linenomath}
\begin{equation} 
\label{eqt:RhoEff} 
\rho_{2}^{eff}(Q) = \frac{\rho_{2}(Q)}{G_{2}(Q)}, 
\end{equation} 
\end{linenomath}

\noindent where $\rho_{2}(Q)$ represents the density distribution constructed on the assumption that Coulomb interactions are absent, while $G_{2}(Q)$ is defined as follows: 

\begin{linenomath}
\begin{equation} 
\label{eqt:GamovFactor} 
G_{2}(Q) = \frac{2 \pi \zeta}{e^{2 \pi \zeta} - 1}, \quad  \text{where} \quad \zeta = \pm \frac{\alpha m}{Q}.
\end{equation} 
\end{linenomath}

\noindent In the above formula $m$ and $\alpha$ denote the particle mass and the fine-structure constant, respectively. The sign of $\zeta$ is positive for like-sign pairs and negative for unlike-sign pairs. According to formula (\ref{eqt:RhoEff}), the Coulomb effect increases with $Q \rightarrow 0$.

Neither the Coulomb interaction nor the BEC effect are present in the generation of MC event samples which are used in the analysis. The Coulomb correction is thus not applied to MC events.

In the present analysis the contribution due to the Coulomb effect can be effectively taken into account by applying the Gamov correction factor to the numerator of the correlation function for data in formula~(\ref{eqt:DR}), since the Coulomb effect is not simulated in MC (but present in data). Fig.~\ref{fig:DR_like_pions_noCoulomb} and Fig.~\ref{fig:DR_unlike_pions_noCoulomb} show the double ratio for like-sign and unlike-sign pion pairs using event-mixed reference sample in three different bins of VELO track multiplicity per $pp$ collision with the Coulomb effect corrected for in the numerator of the correlation functions for data. It may be observed that the $DR$ for unlike-sign pion pairs is flat in the low-$Q$ BEC signal region after applying the Gamov correction factor, and the remaining surplus above 1 is related to the acceptance effects, expected according to the difference in data and MC reconstruction for the track pairs in the low-$Q$ region. This effect is taken into account as a contribution to the systematic uncertainty (see Sect.~\ref{sec:Systematics}) and it has been found to be negligible. For the final fits, the double ratio for like-sign pion pairs with the Coulomb effect subtracted in the correlation function for data has been used.\\

\noindent The majority of the reported BEC analyses take into account the Coulomb interaction effect, whereas the strong final state interactions are usually unaccounted for because of their complexity and insignifficant influence on the fit parameters. 


%FIGURES

\newpage

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.35]{Figures/DoubleRatio_1.eps}
\includegraphics[scale=0.35]{Figures/DoubleRatio_2.eps}
\includegraphics[scale=0.35]{Figures/DoubleRatio_3.eps}
\caption[]
{Double ratio for like-sign pion pairs constructed as described in the text in three different bins of VELO track multiplicity per $pp$ collision: (top left) $<$ 10, (top right) 11-20, (bottom) 21-60.}
\label{fig:DR_pions}
\end{center}
\end{figure}

\newpage

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.35]{Figures/DR_unlike_1.eps}
\includegraphics[scale=0.35]{Figures/DR_unlike_2.eps}
\includegraphics[scale=0.35]{Figures/DR_unlike_3.eps}
\caption[]
{Double ratio for unlike-sign pion pairs constructed as described in the text in three different bins of VELO track multiplicity per $pp$ collision: (top left) $<$ 10, (top right) 11-20, (bottom) 21-60.}
\label{fig:DR_pions_U}
\end{center}
\end{figure}

\newpage

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.35]{Figures/DRC_like_1.eps}
\includegraphics[scale=0.35]{Figures/DRC_like_2.eps}
\includegraphics[scale=0.35]{Figures/DRC_like_3.eps}
\caption[]
{Double ratio for like-sign pion pairs with Coulomb effect subtracted, in three different bins of VELO track multiplicity per $pp$ collision: (top left) $<$ 10, (top right) 11-20, (bottom) 21-60.}
\label{fig:DR_like_pions_noCoulomb}
\end{center}
\end{figure}

\newpage

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.35]{Figures/DRC_unlike_1.eps}
\includegraphics[scale=0.35]{Figures/DRC_unlike_2.eps}
\includegraphics[scale=0.35]{Figures/DRC_unlike_3.eps}
\caption[]
{Double ratio for unlike-sign pion pairs with Coulomb effect subtracted, in three different bins of VELO track multiplicity per $pp$ collision: (top left) $<$ 10, (top right) 11-20, (bottom) 21-60.}
\label{fig:DR_unlike_pions_noCoulomb}
\end{center}
\end{figure}
