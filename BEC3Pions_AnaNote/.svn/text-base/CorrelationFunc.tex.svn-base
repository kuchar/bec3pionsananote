
\section{Correlation functions}
\label{sec:CorrFunc}

As already stated, in particle collisions the space-time structure of the hadronization source can be studied using measurements of Bose-Einstein correlations between pairs of identical particles. Correlations are examined by measuring a two-particle correlation function, defined as a ratio of the inclusive density distribution for two particles and the reference density. The correlation function is commonly studied in the four-momentum difference $Q = \sqrt{-(q_1 - q_2)^2}$:
\begin{equation}
\label{cf_2}
C_{2}(Q) = \frac{\rho_{2}(Q)^{data}}{\rho_{2}(Q)^{ref}},
\end{equation}
\noindent where $Q = \sqrt{-(q_1 - q_2)^2}$ $\rho_{2}(Q)^{data}$ stands for the $Q$ distribution of the like-sign particle pairs and $\rho_{2}(Q)^{ref}$ represents the reference sample, i.e. the same distribution without BEC effects. $\rho_{2}(Q)^{data}$ and $\rho_{2}(Q)^{ref}$ are both normalised to $1$. 

The particle momentum difference $Q$ may be then decomposed into three parts in the Longitudinal Centre-of-Mass coordinate System (LCMS)~\cite{lcms}. The LCMS frame makes possible the separation between spatial and temporal dimensions. LCMS is defined for each pair of particles as a system where the sum of three-vector momenta of two particles is orthogonal to the reference axis (Fig.\ref{fig:lcms}), which may the axis of the physical process, but also a precisely measured axis of the collision. The beam direction ($z$-axis) was chosen in the present analysis as the reference axis. The two-particle system is then rotated in the plane perpendicular to the $\vec{q_{1}} + \vec{q_{2}}$ until the sum of longitudinal components of three-vector momenta amounts to zero. In this coordinate system the variable $\vec{Q} = \vec{q}_{1} - \vec{q}_{2}$ is decomposed into three parts $Q_{long}$, $Q_{t,out}$ and $Q_{t,side}$. The component $Q_{long} \equiv Q_{z}$ is parallel to the reference axis and $Q_{t,out}$ is parallel to the axis collinear with the sum of the paricles' three-vector momenta. $Q_{t,side}$ is perpendicular to both  $Q_{long}$ and $Q_{t,side}$. An important feature of the LCMS frame is that the projection of the total three-vector momentum of the particle pair is equal to $0$ in both \textit{longitudinal} and \textit{side} directions. In the present analysis the spherically symmetric spatial distribution of the particle sources is assumed, thus the boost invariant $Q$ variable is the same in both LHCb laboratory frame as well as the LCMS system.

The distribution of the particle momentum difference $Q$ for the like-sign particle pairs from 2011 mimumum bias events has been compared with three different reference data samples in Fig.~\ref{fig:Q_pions} for preselected pion pairs and in Fig.~\ref{fig:Q_kaons} for kaon pairs. An additional requirement on PID NN $>$ 0.9 for pions and PID NN $>$ 0.7 for kaons has been applied. Correlation functions are determined using three different reference data samples: unlike-sign particle pairs, event-mixed and PV-mixed (see Sect.~\ref{sec:RefSamples}). They are evaluated for preselected pion and kaon pairs as it may be observed in Fig.~\ref{fig:cf_pions} and Fig.~\ref{fig:cf_kaons}. An enhancement in the $C_{2}^(Q)$ distributions in the low $Q$ region for both pion and kaon pairs may be observed.

\subsection{The Coulomb effect}
The final state interactions (FSI) involving both electromagnetic (Coulomb) and strong forces, are present in the Bose-Einstein correlation regions and may potentially affect the distributions of analysed observables. Strong final state interactions are present in both charged and neutral hadron systems, while the Coulomb interactions exert an influence only on the charged hadron systems.

At small $Q$, the Coulomb repulsion between two identically charged hadrons is presumed to alter the correlation function $C_{2}(Q)$ by reducing the enhancement signal. For that reason the so-called 'effective correlation function' is frequently employed: 
\begin{equation} 
\label{eff_corr_func} 
C_{2}^{eff}(Q) = C_{2}(Q) \times G_{2}(Q), 
\end{equation} 
\noindent where $C_{2}(Q)$ represents the correlation function constructed on the assumption that Coulomb interactions are absent, while $G_{2}(Q)$ denotes the Gamov penetration factor \cite{coulomb}, defined as follows: 
\begin{equation} 
\label{Gamov} 
G_{2}(Q) = \frac{2 \pi \alpha m \epsilon_{1} \epsilon_{2}}{Q} \cdot \frac{1}{\exp{(2 \pi \alpha m)} - 1}. 
\end{equation} 
\noindent In the above formula $m$ and $\alpha$ denote the particle mass and the fine structure constant, respectively. The $\epsilon_{1}$ and $\epsilon_{2}$ indicate charges of hadrons (in electron charge magnitude units). According to formula (\ref{eff_corr_func}), the Coulomb effect increases with $Q \rightarrow 0$. It should be noted that the corrections proportional to $1/G_{2}(Q)$, introduced to the real data, may cause an unwanted surplus of the signal~\cite{biya}. Since the Coulomb corrections are significant only in the low $Q$ region, they have to be considered only in a few first bins of measured data distributions.

In the present analysis the contribution due to the Coulomb effect can be effectively subtracted employing the difference in the $Q$ distributions for unlike-sign particle pairs originating from the same or different PV's. The distributions of the particle momentum difference $Q$ for the unlike-sign pairs coming from the same or different PV's for 2011 mimumum bias data sample are shown in Fig.~\ref{fig:Q_coulomb}(a) for pions and Fig.~\ref{fig:Q_coulomb}(b) for kaons. The enhancement indicating the Coulomb effect in the first bins of $Q$ may be clearly visible in Fig.~\ref{fig:cf_coulomb}, where the ratio of the $Q$ distributions for unlike-sign particle pairs originating from the same and different PV's is plotted. The contribution from the Coulomb and other effects in the region of the Bose-Einstein correlation for pions and kaons is shown in Fig.~\ref{fig:coulomb_percentage}. Since other effects like resonances or final state interactions may also affect the ratio of the $Q$ distributions for unlike-sign particle pairs originating from the same and different PV's, the subtraction has been performed only in two first bins, where the Coulomb effect is most expected. The correlation functions with the Coulomb effect subtracted for three different reference data samples are presented in Fig.~\ref{fig:cf_coulombSubtr_pions} for pions and in Fig.~\ref{fig:cf_coulombSubtr_kaons} for kaons, respectively.\\

\noindent The majority of the reported BEC analyses take into account the Coulomb interaction effect, whereas the strong final state interactions are usually unaccounted for because of its complexity and insignifficant influence on the fit parameters. 
 








