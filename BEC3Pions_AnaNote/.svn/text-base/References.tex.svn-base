
\newpage

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.42]{Figures/trkChi2.eps}
\includegraphics[scale=0.42]{Figures/ghostNN.eps}
\caption[]
{(a) $\chi^2$ of the reconstructed track and (b) track probability to be a ghost for 2011 minimum bias data. Red arrows indicate the cuts chosen.}
\label{fig:trkChi2ghostNN}
\end{center}
\end{figure}

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.42]{Figures/p.eps}
\includegraphics[scale=0.42]{Figures/pt.eps}
\caption[]
{(a) Momentum of the track and (b) track transverse momentum for 2011 minimum bias data. Red arrows indicate the cuts chosen.}
\label{fig:trkPPt}
\end{center}
\end{figure}

\newpage

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.42]{Figures/ip.eps}
\includegraphics[scale=0.42]{Figures/ipChi2.eps}
\caption[]
{(a) Track impact parameter and (b) impact parameter $\chi^{2}$ for 2011 minimum bias data. Red arrows indicate the cuts chosen.}
\label{fig:trkIPIPChi2}
\end{center}
\end{figure}

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.42]{Figures/piNN.eps}
\includegraphics[scale=0.42]{Figures/kNN.eps}
\caption[]
{Particle identification probability (PID NN) for (a) pions and (b) kaons for 2011 minimum bias data. Red arrows indicate the cuts chosen.}
\label{fig:piNNkNN}
\end{center}
\end{figure}

\newpage

\begin{figure}[ht]\centering 
\includegraphics[scale=0.80]{Figures/lcms3.eps}
\caption[]
{Longitudinal Centre-of-Mass System.} 
\label{fig:lcms} 
\end{figure} 

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.27]{Figures/QLikeMixEVT_PIONS.eps}
\includegraphics[scale=0.27]{Figures/QLikeMixPV_PIONS.eps}
\includegraphics[scale=0.27]{Figures/QLikeUnlike_PIONS.eps}
\caption[]
{Particles momenta difference $Q$ in the 2011 minimum bias data for like-sign pion pairs (red figures) compared with three different reference samples (black histogram): (a) event-mix, (b) PV-mix, (c) unlike-sign pion pairs. Distrubutions normalized to unity.}
\label{fig:Q_pions}
\end{center}
\end{figure}

\newpage

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.27]{Figures/QLikeMixEVT_KAONS.eps}
\includegraphics[scale=0.27]{Figures/QLikeMixPV_KAONS.eps}
\includegraphics[scale=0.27]{Figures/QLikeUnlike_KAONS.eps}
\caption[]
{Particles momenta difference $Q$ in the 2011 minimum bias data for like-sign kaon pairs (red figures) compared with three different reference samples (black histogram): (a) event-mix, (b) PV-mix, (c) unlike-sign kaon pairs. Distrubutions normalized to unity.}
\label{fig:Q_kaons}
\end{center}
\end{figure}

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.27]{Figures/corrF_mixEVT_PIONS.eps}
\includegraphics[scale=0.27]{Figures/corrF_mixPV_PIONS.eps}
\includegraphics[scale=0.27]{Figures/corrF_unlike_PIONS.eps}
\caption[]
{Two-particle correlation functions for the same sign pion pairs in the 2011 minimum bias data for three different reference samples: (a) event-mix, (b) PV-mix, (c) unlike-sign pion pairs.}
\label{fig:cf_pions}
\end{center}
\end{figure}

\newpage

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.27]{Figures/corrF_mixEVT_KAONS.eps}
\includegraphics[scale=0.27]{Figures/corrF_mixPV_KAONS.eps}
\includegraphics[scale=0.27]{Figures/corrF_unlike_KAONS.eps}
\caption[]
{Two-particle correlation functions for the same sign kaon pairs in the 2011 minimum bias data for three different reference samples: (a) event-mix, (b) PV-mix, (c) unlike-sign kaon pairs.}
\label{fig:cf_kaons}
\end{center}
\end{figure}

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.42]{Figures/QUnlikeUnlike_PIONS.eps}
\includegraphics[scale=0.42]{Figures/QUnlikeUnlike_KAONS.eps}
\caption[]
{Particles momenta difference $Q$ in the 2011 minimum bias data for unlike-sign particle pairs coming from the same (black histogram) or different (red figures) PV's for: (a) pions and (b) kaons. Distrubutions normalized to unity.}
\label{fig:Q_coulomb}
\end{center}
\end{figure}

\newpage

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.42]{Figures/corrF_coulomb_PIONS.eps}
\includegraphics[scale=0.42]{Figures/corrF_coulomb_KAONS.eps}
\caption[]
{The ratio of the $Q$ distributions for unlike-sign particle pairs originating from the same and different PV's for: (a) pions and (b) kaons.}
\label{fig:cf_coulomb}
\end{center}
\end{figure}

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.42]{Figures/corrF_coulomb_PIONS_perc.eps}
\includegraphics[scale=0.42]{Figures/corrF_coulomb_KAONS_perc.eps}
\caption[]
{The contribution from the Coulomb and other effects in the region of the Bose-Einstein correlation for: (a) pions and (b) kaons.}
\label{fig:coulomb_percentage}
\end{center}
\end{figure}

\newpage

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.27]{Figures/corrF_mixEVT_PIONS_AC.eps}
\includegraphics[scale=0.27]{Figures/corrF_mixPV_PIONS_AC.eps}
\includegraphics[scale=0.27]{Figures/corrF_unlike_PIONS_AC.eps}
\caption[]
{The correlation functions for pions with the Coulomb effect subtracted for three different reference data samples: (a) event-mix, (b) PV-mix, (c) unlike-sign pion pairs.}
\label{fig:cf_coulombSubtr_pions}
\end{center}
\end{figure}

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.27]{Figures/corrF_mixEVT_KAONS_AC.eps}
\includegraphics[scale=0.27]{Figures/corrF_mixPV_KAONS_AC.eps}
\includegraphics[scale=0.27]{Figures/corrF_unlike_KAONS_AC.eps}
\caption[]
{The correlation functions for kaons with the Coulomb effect subtracted for three different reference data samples: (a) event-mix, (b) PV-mix, (c) unlike-sign kaon pairs.}
\label{fig:cf_coulombSubtr_kaons}
\end{center}
\end{figure}

\newpage

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.27]{Figures/corrF_mixEVT_PIONS_MC.eps}
\includegraphics[scale=0.27]{Figures/corrF_mixPV_PIONS_MC.eps}
\includegraphics[scale=0.27]{Figures/corrF_unlike_PIONS_MC.eps}
\caption[]
{Correlation functions for pions using the MC sample without the BEC effect for three different reference samples: (a) event-mix, (b) PV-mix, (c) unlike-sign pion pairs.}
\label{fig:cf_MC_pions}
\end{center}
\end{figure}

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.27]{Figures/corrF_mixEVT_KAONS_MC.eps}
\includegraphics[scale=0.27]{Figures/corrF_mixPV_KAONS_MC.eps}
\includegraphics[scale=0.27]{Figures/corrF_unlike_KAONS_MC.eps}
\caption[]
{Correlation functions for kaons using the MC sample without the BEC effect for three different reference samples: (a) event-mix, (b) PV-mix, (c) unlike-sign kaon pairs.}
\label{fig:cf_MC_kaons}
\end{center}
\end{figure}

\newpage

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.27]{Figures/DR_mixEVT_PIONS.eps}
\includegraphics[scale=0.27]{Figures/DR_mixPV_PIONS.eps}
\includegraphics[scale=0.27]{Figures/DR_unlike_PIONS.eps}
\caption[]
{The double ratio of the correlation functions for pions for three different reference samples: (a) event-mix, (b) PV-mix, (c) unlike-sign pion pairs.}
\label{fig:DR_pions}
\end{center}
\end{figure}

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.27]{Figures/DR_mixEVT_KAONS.eps}
\includegraphics[scale=0.27]{Figures/DR_mixPV_KAONS.eps}
\includegraphics[scale=0.27]{Figures/DR_unlike_KAONS.eps}
\caption[]
{The double ratio of the correlation functions for kaons for three different reference samples: (a) event-mix, (b) PV-mix, (c) unlike-sign kaon pairs.}
\label{fig:DR_kaons}
\end{center}
\end{figure}

\newpage

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.35]{Figures/DR_mixEVT_PIONS_FIT_EXP.eps}
\includegraphics[scale=0.35]{Figures/DR_mixPV_PIONS_FIT_EXP.eps}
\includegraphics[scale=0.35]{Figures/DR_unlike_PIONS_FIT_EXP.eps}
\caption[]
{Fit to the double ratio $R(Q)$ for (a) event-mix, (b) PV-mix and (c) unlike-sign pion pairs. Red solid line denotes the fit result using parametrisation described in the text.}
\label{fig:fits_DR_pions}
\end{center}
\end{figure}

\newpage 

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.35]{Figures/DR_mixEVT_KAONS_FIT_EXP.eps}
\includegraphics[scale=0.35]{Figures/DR_mixPV_KAONS_FIT_EXP.eps}
\includegraphics[scale=0.35]{Figures/DR_unlike_KAONS_FIT_EXP.eps}
\caption[]
{Fit to the double ratio $R(Q)$ for (a) event-mix, (b) PV-mix and (c) unlike-sign kaon pairs. Red solid line denotes the fit result using parametrisation described in the text.}
\label{fig:fits_DR_kaons}
\end{center}
\end{figure}



\newpage

\begin{thebibliography}{99}

\bibitem{HBT}
R.~Hanbury~Brown and R.~Q.~Twiss, Phil.~Mag., {\bf 45} (1954) 663.\\
R.~Hanbury~Brown and R~.Q.~Twiss, Nature, {\bf 177} (1956) 27.\\
R.~Hanbury~Brown and R~.Q.~Twiss, Nature, {\bf 178} (1956) 1046.

\bibitem{gglp} 
G.~Goldhaber, W.~B.~Fowler, S.~Goldhaber, T.~F.~Hoang, Phys.~Rev.~Lett. {\bf 3} (1959) 181.\\
G.~Goldhaber, S.~Goldhaber, W.~Y.~Lee and A.~Pais, Phys.~Rev. {\bf 120} (1960) 300.\\
W.~Kittel, Acta~Phys.~Pol. {\bf B32} (2001) 3927. 

\bibitem{expPar}
G.~Kozlov, O.~Utyuzh, G.~Wilk et al., Phys. Atom. Nucl. {\bf 71} (2008) 1502.

\bibitem{ALEPH}
D.~Decamp et al. (ALEPH collaboration), Z.~Phys. {\bf C54} (1992) 75.\\
D.~Buskulic et al. (ALEPH collaboration), Z.~Phys. {\bf 64} (1994) 361.\\
R.~Barate et al. (ALEPH collaboration), Phys.~Lett. {\bf B475} (2000).\\
A. Heister et al. (ALEPH collaboration), Eur.~Phys.~J. {\bf C36} (2004) 147.

\bibitem{DELPHI}
P.~Abreu et al. (DELPHI collaboration), Phys.~Lett. {\bf B286} (1992) 201.\\
P.~Abreu et al. (DELPHI collaboration), Phys.~Lett. {\bf B379} (1996) 330.\\
P.~Abreu et al. (DELPHI collaboration), Phys.~Lett. {\bf B471} (2000) 460.

\bibitem{L3}
M.~Acciarri et al. (L3 collaboration), Phys.~Lett. {\bf B458} (1999) 517.\\
P.~Achard et al. (L3 collaboration), Phys.~Lett. {\bf B524} (2002) 55.\\
P.~Achard et al. (L3 collaboration), Eur.~Phys.~J. {\bf C71} (2011) 1648.

\bibitem{OPAL}
G.~Abbiendi et al. (OPAL collaboration), Eur.~Phys.~J. {\bf C21} (1992) 23.\\
R.~Akers et al. (OPAL collaboration), Z.~Phys.  {\bf C67} (1995) 389.\\
G.~Alexander et al. (OPAL collaboration), Z.~Phys. {\bf C72} (1996) 389.\\
G.~Alexander et al. (OPAL collaboration), Phys.~Lett. {\bf B384} (1996) 377.\\
G.~Abbiendi et al. (OPAL collaboration), Eur.~Phys.~J. {\bf C16} (2000) 423.\\
G.~Abbiendi et al. (OPAL collaboration), Phys.~Lett {\bf B559} (2003) 101.

\bibitem{ALICE}
K.~Aadmond et al., (ALICE collaboration), Phys.~Rev. {\bf D84} (2011) 112004.\\
ALICE collaboration, arXiv:1206.2056.

\bibitem{CMS}
V.~Khachatryan et al. (CMS collaboration, Phys.~Rev.~Lett. {\bf 105} (2010) 032001.\\
V.~Khachatryan et al. (CMS collaboration), JHEP {\bf 1105} (2011) 029.

\bibitem{alexanderModel} 
G.~Alexander, I.~Cohen and E.~Levin, Phys.~Lett. {\bf B452} (1999) 159.

\bibitem{bzModel}
A.~Bialas and K.~Zalewski, Acta.~Phys.~Pol. {\bf B30} (1999) 359.\\
A.~Bialas, M.~Kucharczyk, H.~Palka, K.~Zalewski, Phys~Rev. {\bf D62} (2000) 114007.\\
A.~Bialas, M.~Kucharczyk, H.~Palka, K.~Zalewski, Acta~Phys.~Pol. {\bf B32} (2001) 2901. 

\bibitem{haywood} 
S.~Haywood, {\it Where are we going with Bose-Einstein$-$a Mini Review}, RAL Report {\bf RAL-94-074} (1995).

 \bibitem{tpc} 
H.~Aihara et al. (TPC collaboration), Phys.~Rev. {\bf D31} (1985) 996.\\
H.~Althoff et al. (TASSO collaboration), Z.~Phys. {\bf C30} (1986) 35.\\
I.~Juricic et al. (Mark II collaboration), Phys.~Rev. {\bf D39} (1989) 1.

\bibitem{Pythia} B.~Sj\"ostrand et al., Comput.~Phys.~Commun. {\bf 140}, (2001) 45-55.

\bibitem{TCK} https://twiki.cern.ch/twiki/bin/view/LHCb/TCK

\bibitem{lcms} B.~Andersson, M.~Ringner, Phys.~Lett.~{\bf B421} (1998) 283.

\bibitem{coulomb} M.~Gyulassy et al., Phys.~Rev.~{\bf C20} (1979) 2267.

\bibitem{biya} M.~Biyajima et al., Phys.~Lett.~{\bf B353} (1995) 340. 

\bibitem{tracking_note} A.~Jaeger, P.~Seyfert, M.~De~Cian, S.~Wandernoth, J.~van~Tilburg, S.~Hansmann-Menzemer, {\it Measurement of the track finding efficiency}, LHCB public note, LHCb-PUB-2011-25.



\end{thebibliography}

