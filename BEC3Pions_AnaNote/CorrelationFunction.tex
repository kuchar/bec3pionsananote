
\section{Correlation function}
\label{sec:CorrFunc}

The correlation function studied in the four-momentum difference $Q$ is expressed as:
\begin{linenomath}
\begin{equation}
\label{cf_2}
C_{2}(Q) = \frac{\rho_{2}(Q)^{data}}{\rho_{2}(Q)^{ref}},
\end{equation}
\end{linenomath}
\noindent where $\rho_{2}(Q)^{data}$ stands for the $Q$ distribution of the like-sign particle pairs and $\rho_{2}(Q)^{ref}$ represents the reference sample, i.e. the same distribution without BEC effects. $\rho_{2}(Q)^{data}$ and $\rho_{2}(Q)^{ref}$ are both normalized to $1$.

The correlation function $C_2(Q)$ is commonly parameterized as:
\begin{linenomath}
\begin{equation}
C_2(Q) = N(1\pm \lambda e^{-|R Q|^{\alpha}}) \times (1 + \delta \cdot Q).
\label{eqt:CorrFunctLevy}
\end{equation}
\end{linenomath}
Here the plus or minus sign is valid for bosons or fermions, respectively. $R$ can be considered as a radius of the spherically symmetric source of the emission volume, while $N$ accounts for the overall normalization. In order to account for partial incoherence of the source an additional parameter is required. This parameter is called the chaoticity parameter and is denoted by $\lambda$. It can vary from the value of $0$, in the case of completely coherent source, to the value of $1$ when the source is entirely chaotic. The parameter $\delta$ accounts for long-range momentum correlations. The Levy index of stability $\alpha$ accounts for the assumed density distrubution~\cite{Levy}. In order to take into account possible inter-particle correlations, the Lorentzian (or Cauchy) distribution corresponding to the case of $\alpha = 1$ is used, so the correlation function takes an exponential form:
\begin{linenomath}
\begin{equation}
C_2(Q) = N(1\pm \lambda e^{-|R Q|}) \times (1 + \delta \cdot Q).
\label{eqt:CorrFunctExp}
\end{equation}
\end{linenomath}

Assuming the Gaussian density distribution of the emitter volume, the Levy index of stability $\alpha = 2$, so the two-particle correlation function $C_2$ is parametrized by the Gaussian distribution (the so-called Goldhaber parametrization~\cite{GGLP}):
\begin{linenomath}
\begin{equation}
C_2(Q) = N(1\pm \lambda e^{-R^2 Q^2}) \times (1 + \delta \cdot Q).
\label{eqt:CorrFunctGoldhaber}
\end{equation}
\end{linenomath}

The correlation function is, to first order, independent of the single particle acceptance and efficiency. By construction of the correlation function the effects due to the detector occupancy, acceptance and material are accounted for by dividing the $Q$ distribution for like-sign pion pairs by the reference one.

Especially, since the correlation function in MC without BEC effect is just the ratio of the MC $Q$ distribution for the like-sign particle pairs and the appropriate reference MC $Q$ distribution, there is no necessity to correct the MC distributions like angular distributions of tracks, momentum spectrum of tracks etc. with respect to data since the correction weights will cancel by definition of the correlation function. If we would like to correct the correlation function for MC defined as $C(Q)^{uncorrected}_{MC} = \rho(Q)^{like}_{MC} / \rho(Q)^{ref}_{MC}$, the two-particle weight $w_{2P}(Q)$ may be expressed as a product of two one-particle weights $w_{2P}(Q) = w_{1P}(q_{1}) \times w_{1P}(q_{2})$ for each pair in a given $Q$ bin. Then, the corrected correlation function for MC will be: \mbox{$C(Q)^{corrected}_{MC} = (\rho(Q)^{like}_{MC} \times w_{1P}(q_{1}) \times w_{1P}(q_{2})) / (\rho(Q)^{ref}_{MC} \times w_{1P}(q_{1}) \times w_{1P}(q_{2}))$}. After canceling the weights we have $C(Q)^{corrected}_{MC} = C(Q)^{uncorrected}_{MC}$. Since $Q$ depends on track direction and momentum (particle transverse momentum magnitudes $q_{\bot 1}$, $q_{\bot 2}$, rapidities $y_{1}$, $y_{2}$ and azimuthal angles $\phi_{1}$, $\phi_{2}$), following the formula:
\begin{linenomath}
\begin{equation}
\label{eqt:Q_pt_y_phi}
Q = \sqrt{-2q_{\bot 1}q_{\bot 2}[\cosh(y_{1} - y_{2}) - \cos(\phi_{1}-\phi_{2})]},
\end{equation}
\end{linenomath}
\noindent the weights as a function of $Q$ also depend on track direction and momentum. In any case, the distributions of the basic variables agree reasonably well for data and MC, as it may be seen on the appropriate plots in Sect.~\ref{A} (Appendix A).

The two-particle detector acceptance is a relative acceptance depending on opening angle (so also on $Q$) between two particles in the pair. The effect of the two-particle acceptance may slightly affect the shape of the correlation function. However, the BEC effect is in general an order of magnitude wider than any region affected by this inefficiency and, as a consequence, the extracted radii are not affected by the two-track acceptance. In order to confirm that the correlation function is independent of the possible acceptance inefficiencies, a cross check has been performed, where the double ratio (see Sect.~\ref{sec:DR}) for a sample with artificially modified track acceptance in $\eta$ has been compared with the central result - see Sect.~\ref{sec:Stability}.

The event-mixed reference sample has been used in the present analysis, as it is more precise and less ambiguous than the PV-mixed (PV denotes primary vertex) or unlike reference samples (see Sect.~\ref{sec:RefSamples}). The particle pair is formed from the particles taken from different events from primary vertices with exactly the same VELO track multiplicity. The PV VELO track multiplicity $N_{ch}$ is defined as a number of all reconstructed VELO tracks assigned to the reconstructed PV using the LHCb {\it bestPV} procedure.

Since the correlation radius may depend on the particle multiplicity shown in Fig.~\ref{fig:NchBins}(left), the analysis has been performed in three different bins of VELO track multiplicity per $pp$ collision (reconstructed primary vertex) $N_{ch}$: $<$ 10, (11-20), (21-60). The bins have been chosen to have similar number of particle pairs in each bin (i.e. $\sim$10$^{8}$ particle pairs per bin). Only events with at least five reconstructed VELO tracks have been analysed.

The absolute value of PV multiplicity $N_{ch}$ of reconstructed VELO tracks is not a good tool to compare results among various experiments, in particular when the detector acceptances do not overlap. Therefore, PV activity classes have been defined, being a good probe of the total PV multiplicity in the full solid angle. The advantage of using a relative definition is that the properly defined activity classes are more model independent, allowing for direct comparison of results from different experiments. Three activity classes have been defined as fractions of PV VELO track multiplicity distribution corresponding to the three bins in PV VELO track multiplicity $N_{ch}$ shown in Fig.~\ref{fig:NchBins}(left). They are illustrated in Fig.~\ref{fig:NchBins}(right). The low activity class i.e. (52-100)\% corresponds to a fraction of 48\% of PVs with lowest multiplicities. The medium activity class (15-52)\% contains the 37\% of PVs with higer multiplicities. Finally, the highest activity class (0-15)\% contains 15\% of highest multiplicity PVs.


\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.35]{Figures/multCh.eps}
\includegraphics[scale=0.35]{Figures/activityClasses.eps}
\caption[]
{(Left) VELO track multiplicity per reconstructed PV for 2011 NoBias PV sample (see Sect.~\ref{sec:Samples}) before preselection. (Right) VELO track multiplicity per reconstructed PV for 2011 NoBias PV sample divided into three activity classes defined as fractions of the full distribution, as indicated by colors.}
\label{fig:NchBins}
\end{center}
\end{figure}







