\section{Introduction}
\label{sec:Introduction}

The nature of the multiparticle production within the process of hadronization has been investigated for nearly five decades, but is still not well understood. The Hanbury Brown-Twiss (HBT) intensity interferometry~\cite{HBT} seems to be the only available tool in particle physics experiments which can describe the space-time properties of the hadron emission volume. It relies on quantum interference effect between indistinguishable particles emitted by a finite-size source. In the case of identical bosons the HBT interference effect results in Bose-Einstein Correlations (BEC), while in the case of fermions it is referred to as Fermi-Dirac Correlations (FDC). Both types of correlations are examined by measuring a two-particle correlation function, defined as a ratio of the inclusive density distribution for two particles and the so-called reference density. The latter is a two-particle density distribution which approximates the distribution without the BEC or FDC effects. In a system of two indistinguishable particles emitted from a common source with four momenta $q_1$ and $q_2$ nearby in the phase space, the second order correlation function is defined as~\cite{GGLP}: 
\begin{linenomath} 
\begin{equation} 
\label{eqt:CorrFunctDef} 
C_{2}(q_{1},q_{2}) = \frac{{\mathcal{P}}(q_{1},q_{2})}{{\mathcal{P}}(q_{1}) {\mathcal{P}}(q_{2})} = \frac{{\mathcal{P}}(q_{1},q_{2})}{{\mathcal{P}}_{ref}(q_{1},q_{2})}, 
\end{equation} 
\end{linenomath} 
\noindent where ${\mathcal{P}}(q_{1},q_{2})$ denotes the probability density distribution of two particles with four-momenta $q_{1}$, $q_{2}$, and ${\mathcal{P}}(q_{1})$, ${\mathcal{P}}(q_{2})$ represent probability density distributions of one particle with respective four-momentum $q_{1}$ or $q_{2}$. The denominator in formula (\ref{eqt:CorrFunctDef}) is usually replaced by a certain two-particle density distribution ${\mathcal{P}}_{ref}$ which approximates the distribution without BEC or FDC. 

The correlation function is commonly studied in the four-momentum difference \mbox{$Q = \sqrt{-(q_1 - q_2)^2} = \sqrt{M^2 -4\mu^2}$}, yielding the phase-space separation of the two-particle system, where $M$ represents the invariant mass of the pair of particles and $\mu$ denotes the particle's rest mass.

The main studies of BEC and FDC correlations have been performed at LEP~\cite{ALEPH,DELPHI,L3,OPAL}. Recently, such measurements for pions and kaons have been also reported by LHC experiments for three types of collisions: proton-proton~\cite{ALICEpp,CMSpp,ATLASpp}, proton-lead~\cite{CMSpA} and lead-lead~\cite{CMSpA,ALICEPbPb}. According to these experimental results the source radius of pion pairs varies in the range $(1.0-2.0)~{\rm fm}$, while the sizes of source for kaon and proton pairs are significantly smaller (of the order of 0.5~fm and 0.1~fm, respectively). 

The observed dependence of the correlation radius on the hadron mass, reported by LEP~\cite{ALEPH,DELPHI,L3,OPAL} and LHC~\cite{ALICEpp,CMSpp,ATLASpp,CMSpA,ALICEPbPb} experiments, is interpreted within several theoretical models~\cite{Alexander,BialasZalewski}. One of them~\cite{BialasZalewski} is a quantum-mechanical approach postulating a universal source radius of all particle species and predicting an apparent source size as observed in interferometry measurements, which is smaller than the real size. However, more dependencies have been observed, e.g. it has been reported by different analyses that the correlation radius increases with the charged particle multiplicity, e.g.~\cite{ALICEPbPb}.


