
\section{Systematic uncertainty}
\label{sec:Systematics}

\subsection{Strategy of the treatment of systematic effects}
The raw correlation function for data may be influenced by various acceptance effects due to imperfect reconstruction in the low-$Q$ region where the BEC effect is expected. This is related to the choice of the reference sample. For example the loss of efficiency for two close tracks is expected in data. The reference sample built using mixture of tracks from various events does not contain this inefficiency. Therefore, the usage of the double ratio of correlation functions as described in Sect.~\ref{sec:CorrFunc},\ref{sec:DR} should remove such acceptance effects provided that they are properly modeled in the full simulation. It has to be noted that MC does not need to reproduce perfectly overall (averaged over the whole samples) angular distributions of tracks, momentum spectrum of tracks or PV track multiplicities. These differences cancel by the construction of the correlation function for MC (see Sect.~\ref{sec:CorrFunc}). The example ratio of reference samples for data and MC is shown in Fig~\ref{fig:refRatioDataMC} and it is clearly visible that they are not much different.

To verify that the correlation functions are not sensitive to the overall distributions, the comparison between data and MC for unlike-sign particles, i.e. $\pi^+\pi^-$ pairs, can be performed. They are presented in Fig.~\ref{fig:CF_unlike_DATA} (data) and Fig.~\ref{fig:CF_unlike_MC} (MC) for all PV multiplicities in one plot. For the data the Coulomb effect has been subtracted. 

One can observe that the overall shape is very similar in both samples. It can be seen that the region of low-$Q$ for raw correlation function for data is affected by inefficiency. This inefficiency is very well modeled in simulation. By constructing the double ratio the low-$Q$ inefficiency effect is removed and does not affect the results for BEC parametrization (see Fig.~\ref{fig:DR_unlike_pions_noCoulomb}). In this way all reconstruction effects well modeled in MC may be removed or reduced to negligible level. However, not all effects are properly simulated. One example is the simulation of the reconstruction of clone tracks which may affect the region of $Q$ close to zero. This is discussed in Sect.~\ref{sec:Clones}.

The shapes of correlation functions for total samples in Fig.~\ref{fig:CF_unlike_DATA} and Fig.~\ref{fig:CF_unlike_MC} are similar. One can observe that the negative slope for $Q < 1$~GeV with respect to the flat region of $Q > 1$~GeV is compatible. It can be seen also for like-sign correlation function for MC in Fig.\ref{fig:CF_MC_pions}. Such effects are related to properties of physics generator and proves that overall description in Pythia 8.1 LHCb tuned is correct. However, for special cases like low multiplicity PVs and low $k_{T} = (\vec{p}_{T1} + \vec{p}_{T2}) / 2$ of the pion pair the physics model of the generator may not be correct and the difference in correlation function can appear. This will introduce model dependent systematic effects in double ratio. The distributions of double ratio for the VELO track multiplicity bin ($<$ 10) and $k_{T}$ = $0-1$ GeV is presented in Fig.~\ref{fig:ktVector}. This type of systematic effects are discussed below in the bullet related to the event generators.\\

There are several sources of systematic uncertainties. In principle the result should not be sensitive much to the choice of selection cuts, but we introduced some variations to get the feeling about the impact on the final result and we treat them as a systematic uncertainties. In any case, the dominant source of systematic uncertainty comes from the physics generator model. A list of dominant sources is presented below. The individual contributions to the systematic uncertainty are listed in Tables~\ref{tab:SystTable1}$-$\ref{tab:SystTable3} for three bins of VELO track multiplicity.

\begin{itemize}

\item {\bf different MC generators}\\
To study the systematic uncertainty related to the MC generation the locally produced sample of $\sim$10 M minimum bias events using PYTHIA~6.4 generator with Perugia0 tuning~\cite{Perugia0} has been used to construct double ratio. This particular PYTHIA tuning has been chosen as it has been found that the long-range correlations are well modeled. In Fig.~\ref{fig:Perugia0} the $DR$ distributions constructed using PYTHIA~6.4 Perugia0 MC sample have been fitted using formula~(\ref{eqt:CorrFunctExp}).\\
\hspace*{0.5cm} Since the long-range correlations are treated in different way in the generators based on Lund model (PYTHIA) and cluster model (HERWIG), using HERWIG++ generated MC sample to construct $DR$ may provide additional cross check on the influence of the implementation of long range correlations to the final fit of $DR$ (see Sect.~\ref{D} - Appendix D).

\item {\bf PV reconstruction}\\
The final result may be slightly affected by the PV reconstruction since the like-sign particle pair has to originate from the same PV. Because of the significant pile-up in 2011 data and MC (see Fig.~\ref{fig:MultPV}), and some inefficiency of the PV reconstruction (see Fig.~\ref{fig:EffPVs}), the new tool has been developed on the basis of the mismatch between data and MC. The most important seem to be the cases where the number of reconstructed PV's is less than the number of visible reconstructed MC PV's (visible MC PV is defined as a PV with at least five $hasVelo$ tracks)~\cite{PVNote}. The particle multiplicity seems not to be affected much by this special case (see Fig.~\ref{fig:MultPartsPV}), however an inefficiency of the PV reconstruction as a function of PV multiplicity in the sample with the number of reconstructed PV's less than the number of visible reconstructed MC PV's cannot be completely neglected (see Fig.~\ref{fig:EffPVsMClose}a). This effect becomes much smaller for the events with at least two visible reconstructed MC PV's close to the same reconstructed PV, which corresponds to the more realistic scenario (see Fig.~\ref{fig:EffPVsMClose}b). It may be observed that the inefficiency of the reconstruction of two close PVs are only at the level of $1-2$\% for PV multiplicity equal to 1 or 2, which stands for the majority of the events.\\
\hspace*{0.5cm} In any case, the following procedure has been employed to estimate the systematic uncertainty related to PV reconstruction for both data and MC. The randomly selected tracks have been removed at a conservative rate of 10\% from the initial set of reconstructed tracks in the event. The same algorithm of PV reconstruction as used for the default LHCb PV reconstruction~\cite{PVNote} has been executed for this reduced set of tracks. The analysis has been then repeated with these new reconstructed PVs.\\

\item {\bf PV multiplicity}\\
Another source of systematic uncertainty is related to the PV multiplicity. In Fig.~\ref{fig:VeloTracksPerPV} it may be seen that the number of VELO tracks assigned to the single PV in the cases that there is 1, 2 or $\geq$3 PVs in the event does not differ for both data and MC. The double ratios plotted separately for the events with 1, 2 or $\geq$3 PVs in the three different bins of VELO track multiplicity are gathered in Fig.~\ref{fig:multPVsDRs}. One may observe that in the higher-$Q$ region the slopes are slightly different for cases that we have 1, 2 or $\geq$3 PVs, which is related to possible imperfections of the construction of reference sample, which is mentioned also in the first part of this section. This effect has been already known and it is here included in the systematics as a separate contribution.

\item {\bf ghosts}\\
Since most of the ghosts are already removed by the cuts on the track $\chi^{2}$ and track probability to be a ghost and shared VELO hits, the fraction of ghosts left is at the level of 1\% (see Table~\ref{tab:ghostsFinal}). However, the fraction of ghosts in the collected data may be different as compared to MC sample. Therefore, in order to determine the systematic uncertainty related to the ghosts, the double ratio has been refitted with a very loose cut on the track probability to be a ghost $ProbNN(ghost)$~$<0.50$, being the loosest possible value after preselection. The fraction of ghosts after the final selection using MC truth information for three different bins of VELO track multiplicity and for different cuts on $ProbNN(ghost)<0.25$ and $<0.50$ are gathered in Table~\ref{tab:ghostsFinal}.

\item {\bf clones}\\
The detail discussion about clones is included in Sect.~\ref{sec:Clones}. The fraction of like-sign pion pairs containing a clone after preselection has been determined based on the MC 2011 sample to be $\leq$~1\% (see Table~\ref{tab:SharedVeloClones}). The track clone distance being the Kullback-Liebler distance to the closest track has to be $> 5000$ (log(5000) = 8.52) in the default track reconstruction algorithms. Fig.~\ref{fig:KL} shows the clone distance for all the track pairs in data compared with the same for the ones identified as cloned using Monte Carlo truth. It seems to coincide with the appropriate distributions in~\cite{Needham}. Thus, the systematics related to clones have been determined by refitting the double ratio with event-mixed reference sample for the cut on the logarithm of the clone distance increased to 12.0 in order to eliminate all the cloned pairs visible in MC (see Fig.~\ref{fig:KL}, red histogram). The influence on the fit result has been found insignificant for all bins of VELO track multiplicity. Another contribution to the systematic uncertainty related to the cloned particles has been taken from the fit to the double ratio constructed removing the particle pairs where both particles have $80^{\circ} < \phi < 100 ^{\circ}$ or $260^{\circ} < \phi < 280^{\circ}$. This is to remove the overlap regions between the two detector halves where there are particles giving hits in both halves and the reconstruction produces two slightly different tracks. These clone tracks are not recognized sharing same VELO hits (see Sect.~\ref{C} (Appendix C)), and the momenta might be also different enough that the fail the cut on Kullback-Liebler distance (see Sect.~\ref{sec:Clones}). The influence on the fit result has also been found insignificant for all bins of VELO track multiplicity.

\item {\bf particle identification}\\
Since one of the crucial steps in the selection process, the particle identification can influence the final values of $R$ and $\lambda$ parameters. The failure rate due to empty phase space bin (defined as a ($p$, $\eta$, $Ntracks$) bin where there are no resampling distibutions from PIDCalib) stands for 2.2\%, 0.4\% and 0.2\% for $\pi$, $K$ and $p$, respectively (see Sect.~\ref{sec:PIDCalibration}). In the case of such failure the integrated distributions over track multiplicity have been used. The systematic uncertainty coming from this effect has been estimated by removing from the analysed sample the particles pointing to the empty phase space bin, and refitting the double ratios.

\item {\bf ProbNNpi cut}\\
Since the $ProbNNpi$ cut adjusts the contamination of pions due to misidentification, it can influence the final values of $R$ and $\lambda$ parameters. The contribution of this effect to the systematic uncertainty has been estimated by refitting the $DR$ with the cut on $probNNpi$ ($>$ 0.35) which increases by 50\% the fraction of misidentified pions.

\item {\bf fit range in the low-$Q$ region}\\
The systematics derived from the fit range in the low-$Q$ region has been determined by changing the lower limit of the $Q$ value by $\pm$20\%, i.e. from 0.04 to 0.06 GeV. The fits to the double ratio with two different ranges of $Q$ have been redone in three different bins of VELO track multiplicity.

\item {\bf fit range in the high-$Q$ region}\\
The systematics derived from the fit range in the high-$Q$ region has been determined by changing the upper limit of the $Q$ value by $\pm$10\%, i.e. from 1.8 to 2.2 GeV. The fits to the double ratio with two different ranges of $Q$ have been redone in three different bins of VELO track multiplicity.

\item {\bf fit binning}\\
The systematic uncertainty coming from the different fit binning has been estimated by changing the bin size from 0.003 to 0.005 GeV. It has been found to be negligible for all bins of VELO track multiplicity.

\item {\bf resolution of $Q$ variable}\\
The resolution of $Q$ variable as a function of the $Q$ and sum of momenta of particles from a pair ($p_{1} + p_{2}$) has been plotted in Fig.~\ref{fig:resQ}. As expected, the resolution is increasing from 5-6 MeV for lower $Q$ and $p_{1} + p_{2}$ towards several MeV for higher values. Since the resolution in the low-$Q$ region is at the level of $6-7$~MeV, the effect coming from eventual clone mixing seems to be minor. Looking at the profile plot of the simulated versus reconstructed $Q$ (see Fig.~\ref{fig:QQ}), one can see that there is no special deviation from $Q$ simulated = $Q$ reconstructed curve, especially in the low-$Q$ region.\\
\hspace*{0.5cm} The systematic uncertainty due to resolution of $Q$ variable has been estimated by smearing the $Q$ value within $\pm$1$\sigma$ and refitting final $DR$ distributions. It has been found to be negligible.

\item {\bf Coulomb effect}\\
The systematic uncertainty due to Coulomb corrections is estimated by varying the corrections by $\pm$20\%. It has been found to be negligible.

\item {\bf residual acceptance effects}\\
The possible differences in acceptance effects between DATA and MC in the low-$Q$ region are controlled by looking at the unlike-sign double ratio with Coulomb effect subtracted (see Fig.~\ref{fig:DR_unlike_pions_noCoulomb}), where there is no BEC effect in DATA or MC by definition. One can see that the possible acceptance differences in the low-$Q$ region matter only for the first bin of the VELO track multiplicity. The remaining surplus above 1 in the double ratio distributions for unlike-sign pion pairs with the Gamov correction factor applied (Fig.~\ref{fig:DR_unlike_pions_noCoulomb}) is related to acceptance effects, expected according to the difference in data and MC reconstruction for the track pairs in the low-$Q$ region. To estimate this effect the mentioned surplus in the low-$Q$ region (0.05-0.20) GeV seen in the unlike-sign $DR$ with no Coulomb effect has been subtracted from the final $DR$ of like-sign pion pairs, and the appropriate fits have been redone. It has been found to be negligible.

\end{itemize}

\begin{table}[hbt]
\caption{The fractions of ghosts after the final selection using MC truth information for three different bins of VELO track multiplicity and for different cuts on $ProbNN(ghost)$.}
\begin{tabular}{|c|c|c|c|c|} 
\hline 
cut on $ProbNN(ghost)$ & $N_{ch} < 10$ & $N_{ch} \in (11-20)$ & $N_{ch} \in (21-60)$\\
\hline 
\hline
$<0.25$ & 0.66 [\%] & 0.88 [\%] & 1.20[\%]\\
\hline
$<0.50$ & 0.75 [\%] & 1.01 [\%] & 1.37[\%]\\
\hline
\end{tabular}
\label{tab:ghostsFinal}
\end{table}

\begin{table}[!ht]
\caption[]
{Sources of systematic uncertainty for unfolded charged particle multiplicity bin $N_{ch}^{unfolded} < 18$. Since the parabolic errors are close to the asymmetric ones, only the parabolic ones are quoted. The large relative error on the $\delta$ parameter is due to the fact that its central value is close to zero.}
\begin{tabular}{|c||c|c||c|c|}
\hline
 Source & $\Delta R$ [\%] & $\Delta \lambda$ [\%] & $\Delta \delta$ [\%]\\
\hline
\hline
 MC generators & 6.6 & 4.3 & 48.3\\
\hline
 PV reconstruction & 1.8 & 0.1 & 2.0\\
\hline
 PV multiplicity & 5.9 & 5.8 & 5.1\\
\hline
 ghosts & 0.4 & 1.1 & $< 0.1$\\
\hline
 clones & $< 0.1$ & $< 0.1$ & $< 0.1$\\
\hline
 particle identification & 1.3 & 0.3 & 0.6\\
\hline
 $ProbNNpi$ cut & 2.9 & 1.8 & 4.2\\
\hline
 fit range in low-$Q$ region & 1.2 & 1.0 & 1.4\\
\hline
 fit range in high-$Q$ region & 1.8 & 0.1 & 6.1\\
\hline
 fit binning & $< 0.1$ & $< 0.1$ & $< 0.1$\\
\hline
 resolution of $Q$ variable & $< 0.1$ & $< 0.1$ & $< 0.1$\\
\hline
 Coulomb effect & $< 0.1$ & $< 0.1$ & $< 0.1$\\
\hline
 residual acceptance effects & $< 0.1$ & $< 0.1$ & $< 0.1$\\
\hline
\hline
 total & 9.8 & 7.6 & 49.2\\
\hline
\end{tabular}
\label{tab:SystTable1}
\end{table}

\newpage

\begin{table}[!ht]
\caption[]
{Sources of systematic uncertainty for unfolded charged particle multiplicity bin $N_{ch}^{unfolded} \in (19-35)$. Since the parabolic errors are close to the asymmetric ones, only the parabolic ones are quoted. The large relative error on the $\delta$ parameter is due to the fact that its central value is close to zero.}
\begin{tabular}{|c||c|c||c|c|}
\hline
 Source & $\Delta R$ [\%] & $\Delta \lambda$ [\%] & $\Delta \delta$ [\%]\\
\hline
\hline
 MC generators & 8.9 & 3.5 & 12.6\\
\hline
 PV reconstruction & 1.4 & 1.2 & 3.5\\
\hline
 PV multiplicity & 6.1 & 4.5 & 8.6\\
\hline
 ghosts & 1.7 & 3.9 & 1.6\\
\hline
 clones & $< 0.1$ & $< 0.1$ & $< 0.1$\\
\hline
 particle identification & 0.8 & 0.6 & 2.0\\
\hline
 $ProbNNpi$ cut & 1.6 & 0.1 & 3.8\\
\hline
 fit range in low-$Q$ region & 1.2 & 1.5 & 1.1\\
\hline
 fit range in high-$Q$ region & 2.1 & 0.8 & 8.2\\
\hline
 fit binning & $< 0.1$ & $< 0.1$ & $< 0.1$\\
\hline
 resolution of $Q$ variable & $< 0.1$ & $< 0.1$ & $< 0.1$\\
\hline
 Coulomb effect & $< 0.1$ & $< 0.1$ & $< 0.1$\\
\hline
 residual acceptance effects & $< 0.1$ & $< 0.1$ & $< 0.1$\\
\hline
\hline
 total & 11.4 & 7.3 & 18.3\\
\hline
\end{tabular}
\label{tab:SystTable2}
\end{table}

\begin{table}[!ht]
\caption[]
{Sources of systematic uncertainty for unfolded charged particle multiplicity bin $N_{ch}^{unfolded} \in (36-96)$. Since the parabolic errors are close to the asymmetric ones, only the parabolic ones are quoted. The large relative error on the $\delta$ parameter is due to the fact that its central value is close to zero.}
\begin{tabular}{|c||c|c||c|c|}
\hline
 Source & $\Delta R$ [\%] & $\Delta \lambda$ [\%] & $\Delta \delta$ [\%]\\
\hline
\hline
 MC generators & 6.5 & 1.5 & 37.1\\
\hline
 PV reconstruction & 0.1 & $< 0.1$ & 6.7\\
\hline
 PV multiplicity & 3.9 & 4.3 & 4.0\\
\hline
 ghosts & 1.1 & 0.8 & 2.6\\
\hline
 clones & $< 0.1$ & $< 0.1$ & $< 0.1$\\
\hline
 particle identification & 2.7 & 0.9 & 1.8\\
\hline
 $ProbNNpi$ cut & 1.3 & 0.1 & 0.6\\
\hline
 fit range in low-$Q$ region & 1.8 & 2.7 & 1.6\\
\hline
 fit range in high-$Q$ region & 2.4 & 1.4 & 11.1\\
\hline
 fit binning & $< 0.1$ & $< 0.1$ & $< 0.1$\\
\hline
 resolution of $Q$ variable & $< 0.1$ & $< 0.1$ & $< 0.1$\\
\hline
 Coulomb effect & $< 0.1$ & $< 0.1$ & $< 0.1$\\
\hline
 residual acceptance effects & $< 0.1$ & $< 0.1$ & $< 0.1$\\
\hline
\hline
 total & 8.8 & 5.6 & 39.6\\
\hline
\end{tabular}
\label{tab:SystTable3}
\end{table}


\subsection{Stability cross checks}
\label{sec:Stability}

The results should be, to the first order, independent of the single particle acceptance and efficiency (see Sect.~\ref{sec:CorrFunc}). However, the stability of the results with respect to the choice of selection cuts has been checked.

\begin{itemize}

\item {\bf track reconstruction/acceptance}\\
In order to confirm that the correlation function is independent of possible acceptance inefficiencies, an additional cross check has been performed, where the double ratio for the sample with artificially modified track acceptance in $\eta$ has been compared with the central result. The procedure is as follows: {\it (i)} we introduced additional inefficiency in track $\eta$, assuming that we have 100\% efficiency for tracks with $\eta = 2$, and this efficiency is linearly going down to 80\% for tracks with $\eta = 5$ (f($\eta$) = -0.065 $\times$ $\eta$ + 1.13); {\it (ii)} for each track we have been randomly drawing the value in the range (0-1), and if the drawn value has been greater than -0.065 $\times$ $\eta$ + 1.13, we have removed such a track from the sample; {\it (iii)} for such acceptance modified sample we have constructed the double ratio and compare the result with the DR without any modifications of this kind. As it may be seen in Fig.~\ref{fig:DR_Acc} the distributions agree very well, thus there is no influence of the possible acceptance inefficiency dependence on the measured correlation radius or chaoticity parameter.

\item {\bf magnet polarity}\\
To search for possible differences between the 2011 datasets collected with two different magnet polarities ($MagUp$ and $MagDown$)~\cite{Koppenburg}, the double ratio distribution was fitted separately for MagUp and MagDown. The difference of the fit results between MagUp and MagDown for all fit parameters is found to be within statistical errors. Furthermore, the cross check for sub-samples corresponding to different data taking periods (i.e. before and after technical stop) has been also performed to check the stability of the result. As it may be observed in Fig.~\ref{fig:TS}, the results do not depend on data taking periods. The dependence of the correlation radius on the different data taking periods (i.e. before and after technical stop) in the event for the VELO track multiplicity bin (11-20) is illustrated in Fig.~\ref{fig:graphTS}.

\item {\bf fitting procedure}\\
The fit procedure has been checked against possible bias using toy MC technique. The individual $Q$ distributions for data and for MC have been generated using the typical distribution derived from data. The BEC effect was implemented by removing events according to probability corresponding to Levi parametrization~(\ref{eqt:CorrFunctLevy}) in the numerator of the corelation function for data. The generated number of particle pairs corresponds to the statistics used in the measurements. Then the double ratio has been constructed and the fit has been performed. The distributions of fitted values and pulls for $R$ and $\lambda$ parameters obtained from 1000 MC toys are presented in Fig.~\ref{fig:toyValues} and Fig.~\ref{fig:toyPulls}, respectively. No indication of any bias is seen.

\item {\bf IP cut}\\
A very loose cut on the track IP (0.40 mm) has been finally applied resulting in the small difference with respect to other tried values. Applying tight cut on the track IP ($< 0.17$~mm) one can observe only a minor difference as compared to the final result with IP~$< 0.40$~mm (see Fig.~\ref{fig:ip04}). Thus, there is no significant influence of the IP cut on the measured correlation radius or chaoticity parameter.

\item {\bf Contamination from beam-gas interactions}\\
The possible contamination of the PVs originating from the beam-gas interactions has been also estimated by counting how many such PVs are accepted in the event being marked with the ODIN flag as a beam-gas. Assuming that the probability to observe the beam-gas PV in the beam-beam crossings is the same as in the beam-gas crossings, this number has been extrapolated to the number of all beam-beam crossings. It has been found to be negligible (the probability to accept the beam-gas PV after preselection cuts was estimated to be less than 10$^{-5}$).

\item {\bf Contamination from mixed pairs}\\
In the context of the BEC analysis the contamination from the mixed pairs in the like-sign pion sample (e.g. pion-kaon or pion-proton pairs misidentified as a pion pair) is not considered to be a background as it cancels by definition in the double ratio almost perfectly. In order to account for such an effect, the correlation function for MC has been constructed excluding mixed pairs defined above. Then the double ratio has been determined, where the correlation function for data has been untouched. As we can see in Fig.~\ref{fig:DR_MixedPairs}, there is no change in double ratio shape after removing mixed pairs from the MC correlation function, and the fitted parameters stay the same within much less than 1\%.

\end{itemize}


\clearpage

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.35]{Figures/EVMIX_RDtoMC_1.eps}
\includegraphics[scale=0.35]{Figures/EVMIX_RDtoMC_2.eps}
\includegraphics[scale=0.35]{Figures/EVMIX_RDtoMC_3.eps}
\caption[]
{Ratio of reference samples for data and MC in three different bins of VELO track multiplicity per $pp$ collision: (top left) $<$ 10, (top right) 11-20, (bottom) 21-60.}
\label{fig:refRatioDataMC}
\end{center}
\end{figure}

\newpage

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.35]{Figures/corrFunctU_RD_1.eps}
\includegraphics[scale=0.35]{Figures/corrFunctU_RD_2.eps}
\includegraphics[scale=0.35]{Figures/corrFunctU_RD_3.eps}
\caption[]
{Correlation function for unlike-sign pion pairs from data with event-mixed reference sample in three different bins of VELO track multiplicity per $pp$ collision: (top left) $<$ 10, (top right) 11-20, (bottom) 21-60.}
\label{fig:CF_unlike_DATA}
\end{center}
\end{figure}

\newpage

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.35]{Figures/corrFunctU_MC_1.eps}
\includegraphics[scale=0.35]{Figures/corrFunctU_MC_2.eps}
\includegraphics[scale=0.35]{Figures/corrFunctU_MC_3.eps}
\caption[]
{Correlation function for unlike-sign pion pairs from MC sample with event-mixed reference sample in three different bins of VELO track multiplicity per $pp$ collision: (top left) $<$ 10, (top right) 11-20, (bottom) 21-60.}
\label{fig:CF_unlike_MC}
\end{center}
\end{figure}

\newpage

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.35]{Figures/DRFitP0_pion_1.eps}
\includegraphics[scale=0.35]{Figures/DRFitP0_pion_2.eps}
\includegraphics[scale=0.35]{Figures/DRFitP0_pion_3.eps}
\caption{Results of the fit to the double ratio for like-sign pion pairs with the Coulomb effect subtracted using Pythia~6.4 MC simulation with Perugia0 tuning in three different bins of VELO track multiplicity per $pp$ collision: (top left) 2-10, (top right) 11-20, (bottom) 21-60. Red solid line denotes the fit result using parametrization described in the text.}
\label{fig:Perugia0}
\end{center}
\end{figure}


\newpage


\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.40]{Figures/multPV.eps}
\includegraphics[scale=0.40]{Figures/multPV_MC.eps}
\caption[]
{Multiplicity of PVs for 2011 data (left) and MC (right).}
\label{fig:MultPV}
\end{center}
\end{figure}

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.42]{Figures/effPVs.eps}
\includegraphics[scale=0.42]{Figures/effPVsP.eps}
\caption[]
{(a) Efficiency of the PV reconstruction as a function of PV multiplicity in the 2011 data events with the same number of reconstructed and visible MC PV's. (b) Inefficiency of the PV reconstruction as a function of PV multiplicity in the 2011 data events with the number of reconstructed PV's greater than the number of visible MC PV's. In both cases red lines denote events before preselection while the blue ones the events after preselection.}
\label{fig:EffPVs}
\end{center}
\end{figure}

\newpage 

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.40]{Figures/multParts_LIN.eps}
\includegraphics[scale=0.40]{Figures/multParts_LOG.eps}
\caption[]
{Comparison between particle multiplicity in the 2011 data events after preselection (red histogram) and the events with the number of reconstructed PV's less than the number of visible MC PV's (black figures) in (a) linear and (b) logarithmic scale.}
\label{fig:MultPartsPV}
\end{center}
\end{figure}

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.40]{Figures/effPVsM.eps}
\includegraphics[scale=0.40]{Figures/effPVsMClose.eps}
\caption[]
{(a) Inefficiency of the PV reconstruction as a function of PV multiplicity in the 2011 data events with the number of reconstructed PV's less than the number of visible MC PV's. (b) The same as in (a) but for the events with at least two visible MC PV's close to the same reconstructed PV. In both cases red lines denote events before preselection while the blue ones the events after preselection.}
\label{fig:EffPVsMClose}
\end{center}
\end{figure}

\newpage

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.40]{Figures/multPV_RD_NB.eps}
\includegraphics[scale=0.40]{Figures/multPV_MC_NB.eps}
\caption[]
{Number of VELO tracks assigned to the single PV in the cases that there is 1 (red), 2 (blue) or $\geq$3 (yellow) PVs in the event, for NoBias PV data sample (left) and MC (right).}
\label{fig:VeloTracksPerPV}
\end{center}
\end{figure}

\newpage

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.35]{Figures/DRFit_pion_1PV.eps}
\includegraphics[scale=0.35]{Figures/DRFit_pion_2PV.eps}
\includegraphics[scale=0.35]{Figures/DRFit_pion_3PV.eps}
\caption{Results of the fit to the double ratio for like-sign pion pairs with event-mixed reference sample and the Coulomb effect subtracted in three bins of VELO track multiplicity per $pp$ collision: $<$ 10 (top left), 11-20 (top right), 21-60 (bottom) for the events with 1 (blue), 2 (red) and $\geq$3 (green) PVs.}
\label{fig:multPVsDRs}
\end{center}
\end{figure}

\newpage

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.40]{Figures/cloneDist.eps}
\caption[]
{Clone distance for all the track pairs in data (black) compared with the same for the ones identified as cloned using Monte Carlo truth (red). The distributions are normalized to the same number of events.}
\label{fig:KL}
\end{center}
\end{figure}


\newpage

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.35]{Figures/resQQF.eps}
\includegraphics[scale=0.35]{Figures/resQPF.eps}
\caption[]
{(left) The resolution of $Q$ variable as a function of the $Q$. (right) The resolution of $Q$ variable as a function of the sum of momenta of particles from a pair ($p_{1} + p_{2}$).}
\label{fig:resQ}
\end{center}
\end{figure}

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.40]{Figures/resQF.eps}
\caption[]
{Profile plot of the simulated versus reconstructed $Q$. Red line indicated the case where $Q$ simulated = $Q$ reconstructed.}
\label{fig:QQ}
\end{center}
\end{figure}

\newpage

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.35]{Figures/DR_eta_acc3.eps}
\includegraphics[scale=0.35]{Figures/DR_eta_acc2.eps}
\includegraphics[scale=0.35]{Figures/DR_eta_acc1.eps}
\caption{Double ratio for like-sign pion pairs with event-mixed reference sample and the Coulomb effect subtracted in three different bins of VELO track multiplicity per $pp$ collision: (top left) $<$ 10, (top right) 11-20, (bottom) 21-60, for the case without any modification of the track acceptance in $\eta$ (blue) and with artificially introduced inefficiency of the track acceptance in $\eta$, as described in the text (red).}
\label{fig:DR_Acc}
\end{center}
\end{figure}

\newpage

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.35]{Figures/TS1.eps}
\includegraphics[scale=0.35]{Figures/TS2.eps}
\includegraphics[scale=0.35]{Figures/TS3.eps}
\caption{Double ratio for like-sign pion pairs with event-mixed reference sample and the Coulomb effect subtracted in three bins of VELO track multiplicity per $pp$ collision: $<$ 10 (top left), 11-20 (top right), 21-60 (bottom) for sub-samples corresponding to different data taking periods, before (blue) and after (red) technical stop.}
\label{fig:TS}
\end{center}
\end{figure}

\newpage

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.30]{Figures/radius_TS.eps}
\includegraphics[scale=0.30]{Figures/lambda_TS.eps}
\caption[]
{(left) Correlation radius and (right) chaoticity parameter as a function of data taking period, i.e. before (left point) and after (right point) technical stop.}
\label{fig:graphTS}
\end{center}
\end{figure}

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.50]{Figures/toyValues.eps}
\caption{The distributions of $R$ (upper) and $\lambda$ (bottom) parameters obtained for 1000 MC toys, as described in the text.}
\label{fig:toyValues}
\end{center}
\end{figure}

\newpage

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.50]{Figures/toyPulls.eps}
\caption{The pull distributions of $R$ (upper) and $\lambda$ (bottom) parameters obtained for 1000 MC toys, as described in the text.}
\label{fig:toyPulls}
\end{center}
\end{figure}

\newpage

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.35]{Figures/DR_IP04_1.eps}
\includegraphics[scale=0.35]{Figures/DR_IP04_2.eps}
\includegraphics[scale=0.35]{Figures/DR_IP04_3.eps}
\caption{Double ratio for like-sign pion pairs with event-mixed reference sample and the Coulomb effect subtracted for IP~$< 0.17$~mm (blue) and IP~$< 0.40$~mm (red) in three different bins of VELO track multiplicity per $pp$ collision: (top left) $<$ 10, (top right) 11-20, (bottom) 21-60.}
\label{fig:ip04}
\end{center}
\end{figure}

\newpage

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.35]{Figures/DR_mixedPairs_1.eps}
\includegraphics[scale=0.35]{Figures/DR_mixedPairs_2.eps}
\includegraphics[scale=0.35]{Figures/DR_mixedPairs_3.eps}
\caption{Double ratio for like-sign pion pairs with event-mixed reference sample and the Coulomb effect subtracted in three different bins of VELO track multiplicity per $pp$ collision: (top left) $<$ 10, (top right) 11-20, (bottom) 21-60, for the case without any modification of the double ratio (blue) and for double ratio constructed using MC correlation function where the mixed-pairs have been removed (red), as described in the text.}
\label{fig:DR_MixedPairs}
\end{center}
\end{figure}


