
\section{Selection}
\label{sec:Selection}

An additional requirement on the number of shared VELO hits has been applied, i.e. if there are two or more tracks that share all hits in the VELO detector, then the only one of them with the best track $\chi^{2}$ is retained. This requirement reduces the fraction of ghosts (tracks not matched to any MC true particle using LHCb MC linker) and clones. If such tracks are in the pair, then, with a high probability, they create a pair with very low value of the $Q$ variable. This may affect the shape of the correlation function in the BEC signal region. See Sect.~\ref{C} (Appendix C) for details.


\subsection{Resampling of particle identification for simulated sample}
\label{sec:PIDCalibration}

The particle identification is not simulated perfectly. The $ProbNN$ variables have to be calibrated before applying the identification criteria to ensure agreement between data and simulation. Therefore, it is necessary to perform the so-called MC resampling, which makes corrections in the PID variable distributions in MC sample. There are tools dedicated for this purpose in the LHCb software, i.e. {\it PIDCalib} package~\cite{PIDCalib}. $ProbNN$ variables depend mainly on three parameters: track’s momentum $p$, pseudorapidity $\eta$ and the number of tracks in the event {\it Ntracks}. For this reason, 3-dimensional ($p$, $\eta$, $Ntracks$) weights are created and applied in further steps to correct the $ProbNN$ distributions. Default $PIDCalib$ scripts are dedicated for corrections of the products of a given decay candidate. In the case of particle correlations all charged tracks in an event have to be recalibrated. Therefore, the default $PIDCalib$ scripts have been modified with respect to the needs of BEC analysis. For each reconstructed track corresponding to a given MC particle the $ProbNN$ values from calibration sample based on real data are drawn from appropriate distributions (e.g. from pion calibration sample for the true MC pion). In this way improperly simulated values of $ProbNNs$ are replaced by the correct ones based on data. The corrected $ProbNN$ values ($\pi$, $K$, $p$, $\mu$, $e$) are written to the $ntuple$ and they are used in the analysis instead of originally simulated ones. The standard binning in momentum, pseudorapidity and charged track multiplicity was used. To correct for the difference between data and MC, the number of tracks in simulated event is increased by a factor of 1.3 before choosing the corresponding $PIDCalib$ distribution. The phase space coverage for pions and kaons from minimum bias events is satisfactory. The list of calibration samples was extended by a sample of $\Lambda \to p\pi$ to enrich the statistics for protons in low momentum bins. If the particle is a ghost (i.e. its MC ID amounts zero) the originally simulated PID probability has been taken. The failure rate due to the empty phase space bin, defined as a ($p$, $\eta$, $Ntracks$) bin where there are no resampling distibutions from PIDCalib, is 2.2\%, 0.4\% and 0.2\% for $\pi$, $K$ and $p$, respectively. In the case of such failure the integrated distributions over track multiplicity are used. This reduces the failure rate below 0.1\% for $\pi$, $K$ and $p$. A higher level of failures is observed for muons and electrons. However, the number of muons and electrons misidentified as pions is low (see Tables~\ref{tab:Purity1}-\ref{tab:Purity3}). The corresponding contamination of $\pi \mu$ or $\pi e$ pairs misidentified as $\pi \pi$ is very low, i.e. 0.25\% for $\pi \mu$ and 0.24\% for $\pi e$. The same, by definition of the correlation function the rate of like-sign pion pairs, where at least one of the tracks is classified as a muon or electrons, should be similar in the numerator and in denominator, so the effect tends to cancel out. This effect is included in the systematics related to the particle identification (see Sect.~\ref{sec:Systematics}). 

The distributions of $probNNpi$, $probNNk$ and $probNNp$ variables for the MC pion sample before and after PID calibration are shown in Fig.~\ref{fig:PIDCalib}.


\subsubsection{Optimization of pion identification}
\label{sec:OptimizationProbNNpi}

To avoid eventual second order distortions on the double ratio related to the differencies between data and simulation the requirement on the $probNNpi$ variable has to be chosen optimally. One has to ensure that the purity of the pion sample is high, but on the other hand, a strong requirement on the $probNNpi$ may affect the signal region at low-$Q$. All the results shown in the present note on the correlation function and double ratio have additional vetoes on kaon and proton identification, i.e. $probNNk <0,5$ and $probNNp < 0.5$. As it may be seen in Tables~\ref{tab:Purity1}-\ref{tab:Purity3}, the purity of the pion sample increases with the value of the $probNNpi$ limit, however it varies only within 1\%. The efficiencies of the requirement on the $probNNpi$ for a single pion are summarized in Tables~\ref{tab:EffPi1}-\ref{tab:EffPi3}. The effect of the cut on $ProbNNpi$ on the shape of the correlation function for the data and Monte Carlo samples is shown in Fig.~\ref{fig:PurityData} and Fig.~\ref{fig:PurityMC}, respectively. A stronger cut on $ProbNNpi$ has no influence on the shape of the correlation function for the Monte Carlo sample, except in the first bin. Since in the low-$Q$ region the separation in momentum between two particles is poor and the difference in data and MC reconstruction for the track pairs is significant, the region of $Q < 0.05$ GeV has been excluded from the further analysis (see Sect.~\ref{sec:FinalSelection}). As the effect related to the cut on $ProbNNpi$ is much more stronger in the data, it means that a very strict requirement on $probNNpi$ may reject the region of the phase-space where the signal pairs are expected. The optimal limit has been chosen to $probNNpi > 0.65$, as for such a value the signal enhancement in the low-$Q$ region for the data begins to saturate. Moreover, the purity of the pion sample for such a cut stays at acceptable level. Tables~\ref{tab:PurityPairs1}-\ref{tab:PurityPairs3} present the fractions of pion, kaon and proton like-sign pairs in the pion sample in the BEC signal region of $Q < 1.0$~GeV, compared with the pairs of different types (e.g. pion-kaon). It may be seen that the rate of kaon-kaon and proton-proton pairs in the pion sample is negligible, thus they do not affect the BEC signal for pions (the pairs of different types do not affect the BEC signal by definition).

\begin{table}[hbt]
\caption{Fractions of different particle species in the true pion sample for various cuts on $ProbNNpi$ variable in the bin of VELO track multiplicity per $pp$ collision $N_{ch} < 10$. Statistical uncertainties are not quoted since they are negligible. This study is performed for MC data sample.}
\begin{tabular}{|c|c|c|c|c|c|c|c|} 
\hline 
ProbNNpi & pions [\%] & kaons [\%] & protons [\%] & electrons [\%] & muons [\%] & ghosts [\%]\\
\hline 
\hline
$> 0.50$ & 97.93 & 0.67 & 0.42 & 0.15 & 0.21 & 0.62\\
\hline
$> 0.55$ & 98.09 & 0.63 & 0.36 & 0.12 & 0.19 & 0.61\\
\hline
$> 0.60$ & 98.25 & 0.56 & 0.32 & 0.10 & 0.17 & 0.60\\
\hline
$> 0.65$ & 98.40 & 0.50 & 0.27 & 0.09 & 0.15 & 0.59\\
\hline
$> 0.70$ & 98.56 & 0.44 & 0.22 & 0.07 & 0.13 & 0.58\\
\hline
$> 0.75$ & 98.72 & 0.39 & 0.18 & 0.05 & 0.11 & 0.56\\
\hline
$> 0.80$ & 98.86 & 0.35 & 0.13 & 0.04 & 0.09 & 0.53\\
\hline 
\end{tabular}
\label{tab:Purity1}
\end{table}

\begin{table}[hbt]
\caption{Fractions of different particle species in the true pion sample for various cuts on $ProbNNpi$ variable in the bin of VELO track multiplicity per $pp$ collision $N_{ch} \in (11-20)$. Statistical uncertainties are not quoted since they are negligible. This study is performed for MC data sample.}
\begin{tabular}{|c|c|c|c|c|c|c|c|} 
\hline 
ProbNNpi & pions [\%] & kaons [\%] & protons [\%] & electrons [\%] & muons [\%] & ghosts [\%]\\
\hline 
\hline
$> 0.50$ & 97.56 & 0.76 & 0.46 & 0.20 & 0.19 & 0.83\\
\hline
$> 0.55$ & 97.75 & 0.70 & 0.40 & 0.16 & 0.17 & 0.82\\
\hline
$> 0.60$ & 97.94 & 0.62 & 0.35 & 0.13 & 0.15 & 0.80\\
\hline
$> 0.65$ & 98.12 & 0.55 & 0.29 & 0.12 & 0.14 & 0.78\\
\hline
$> 0.70$ & 98.30 & 0.48 & 0.24 & 0.10 & 0.12 & 0.76\\
\hline
$> 0.75$ & 98.50 & 0.41 & 0.19 & 0.07 & 0.10 & 0.73\\
\hline
$> 0.80$ & 98.68 & 0.34 & 0.15 & 0.06 & 0.08 & 0.69\\
\hline 
\end{tabular}
\label{tab:Purity2}
\end{table}

\newpage

\begin{table}[hbt]
\caption{Fractions of different particle species in the true pion sample for various cuts on $ProbNNpi$ variable in the bin of VELO track multiplicity per $pp$ collision $N_{ch} \in (21-60)$. Statistical uncertainties are not quoted since they are negligible. This study is performed for MC data sample.}
\begin{tabular}{|c|c|c|c|c|c|c|c|} 
\hline 
ProbNNpi & pions [\%] & kaons [\%] & protons [\%] & muons [\%] & electrons [\%] & ghosts [\%]\\
\hline 
\hline
$> 0.50$ & 96.87 & 0.99 & 0.61 & 0.23 & 0.18 & 1.13\\
\hline
$> 0.55$ & 97.11 & 0.89 & 0.54 & 0.19 & 0.16 & 1.11\\
\hline
$> 0.60$ & 97.36 & 0.79 & 0.47 & 0.16 & 0.14 & 1.09\\
\hline
$> 0.65$ & 97.58 & 0.69 & 0.40 & 0.14 & 0.12 & 1.06\\
\hline
$> 0.70$ & 97.82 & 0.59 & 0.33 & 0.12 & 0.11 & 1.02\\
\hline
$> 0.75$ & 98.09 & 0.48 & 0.26 & 0.09 & 0.09 & 0.98\\
\hline
$> 0.80$ & 98.37 & 0.38 & 0.20 & 0.07 & 0.07 & 0.91\\
\hline 
\end{tabular}
\label{tab:Purity3}
\end{table}

\begin{table}[hbt]
\caption{
The efficiency of the requirement on the $probNNpi$ for a single pion for various cuts on $ProbNNpi$ variable in the bin of VELO track multiplicity per $pp$ collision $N_{ch} < 10$. Statistical uncertainties are not quoted since they are negligible. This study is performed for MC data sample.}
\begin{tabular}{|c|c|c|c|c|c|c|c|} 
\hline 
ProbNNpi & Single pion efficiency [\%]\\
\hline 
\hline
$> 0.50$ & 95.63\\
\hline
$> 0.55$ & 95.02\\
\hline
$> 0.60$ & 94.37\\
\hline
$> 0.65$ & 93.54\\
\hline
$> 0.70$ & 92.24\\
\hline
$> 0.75$ & 90.86\\
\hline
$> 0.80$ & 88.77\\
\hline 
\end{tabular}
\label{tab:EffPi1}
\end{table}

\newpage

\begin{table}[hbt]
\caption{
The efficiency of the requirement on the $probNNpi$ for a single pion for various cuts on $ProbNNpi$ variable in the bin of VELO track multiplicity per $pp$ collision $N_{ch} \in (11-20)$. Statistical uncertainties are not quoted since they are negligible. This study is performed for MC data sample.}
\begin{tabular}{|c|c|c|c|c|c|c|c|} 
\hline 
ProbNNpi & Single pion efficiency [\%]\\
\hline 
\hline
$> 0.50$ & 95.32\\
\hline
$> 0.55$ & 94.66\\
\hline
$> 0.60$ & 93.92\\
\hline
$> 0.65$ & 92.99\\
\hline
$> 0.70$ & 91.65\\
\hline
$> 0.75$ & 90.12\\
\hline
$> 0.80$ & 87.92\\
\hline 
\end{tabular}
\label{tab:EffPi2}
\end{table}

\begin{table}[hbt]
\caption{
The efficiency of the requirement on the $probNNpi$ for a single pion for various cuts on $ProbNNpi$ variable in the bin of VELO track multiplicity per $pp$ collision $N_{ch} \in (21-60)$. Statistical uncertainties are not quoted since they are negligible. This study is performed for MC data sample.}
\begin{tabular}{|c|c|c|c|c|c|c|c|} 
\hline 
ProbNNpi & Single pion efficiency [\%]\\
\hline 
\hline
$> 0.50$ & 94.75\\
\hline
$> 0.55$ & 93.95\\
\hline
$> 0.60$ & 93.02\\
\hline
$> 0.65$ & 91.89\\
\hline
$> 0.70$ & 90.38\\
\hline
$> 0.75$ & 88.54\\
\hline
$> 0.80$ & 86.01\\
\hline 
\end{tabular}
\label{tab:EffPi3}
\end{table}

\newpage

\begin{table}[hbt]
\caption{Fractions of pion, kaon and proton like-sign pairs in the true pion sample in the BEC signal region of $Q < 1.0$~GeV, compared with the pairs of different types (e.g. pion-kaon) for various cuts on $ProbNNpi$ variable in the bin of VELO track multiplicity per $pp$ collision $N_{ch} < 10$. Statistical uncertainties are not quoted since they are negligible. This study is performed for MC data sample.}
\begin{tabular}{|c|c|c|c|c|} 
\hline 
ProbNNpi & pion pairs [\%] & kaon pairs [\%] & proton pairs [\%] & mixed pairs [\%]\\
\hline 
\hline
$> 0.50$ & 96.151 & 0.006 & 0.002 & 3.840\\
\hline
$> 0.55$ & 96.474 & 0.005 & 0.002 & 3.518\\
\hline
$> 0.60$ & 96.821 & 0.004 & 0.001 & 3.173\\
\hline
$> 0.65$ & 97.121 & 0.004 & $<$ 0.001 & 2.874\\
\hline
$> 0.70$ & 97.443 & 0.003 & $<$ 0.001 & 2.553\\
\hline
$> 0.75$ & 97.766 & 0.002 & $<$ 0.001 & 2.231\\
\hline
$> 0.80$ & 98.056 & 0.001 & $<$ 0.001 & 1.942\\
\hline 
\end{tabular}
\label{tab:PurityPairs1}
\end{table}

\begin{table}[hbt]
\caption{Fractions of pion, kaon and proton like-sign pairs in the true pion sample in the BEC signal region of $Q < 1.0$~GeV, compared with the pairs of different types (e.g. pion-kaon) for various cuts on $ProbNNpi$ variable in the bin of VELO track multiplicity per $pp$ collision $N_{ch} \in (11-20)$. Statistical uncertainties are not quoted since they are negligible. This study is performed for MC data sample.}
\begin{tabular}{|c|c|c|c|c|} 
\hline 
ProbNNpi & pion pairs [\%] & kaon pairs [\%] & proton pairs [\%] & mixed pairs [\%]\\
\hline 
\hline
$> 0.50$ & 95.719 & 0.008 & 0.003 & 4.270\\
\hline
$> 0.55$ & 96.091 & 0.006 & 0.002 & 3.900\\
\hline
$> 0.60$ & 96.474 & 0.005 & 0.002 & 3.519\\
\hline
$> 0.65$ & 96.819 & 0.004 & 0.001 & 3.175\\
\hline
$> 0.70$ & 97.183 & 0.003 & $<$ 0.001 & 2.813\\
\hline
$> 0.75$ & 97.566 & 0.002 & $<$ 0.001 & 2.431\\
\hline
$> 0.80$ & 97.925 & 0.001 & $<$ 0.001 & 2.073\\
\hline 
\end{tabular}
\label{tab:PurityPairs2}
\end{table}

\begin{table}[hbt]
\caption{Fractions of pion, kaon and proton like-sign pairs in the true pion sample in the BEC signal region of $Q < 1.0$~GeV, compared with the pairs of different types (e.g. pion-kaon) for various cuts on $ProbNNpi$ variable in the bin of VELO track multiplicity per $pp$ collision $N_{ch} \in (21-60)$. Statistical uncertainties are not quoted since they are negligible. This study is performed for MC data sample.}
\begin{tabular}{|c|c|c|c|c|} 
\hline 
ProbNNpi & pion pairs [\%] & kaon pairs [\%] & proton pairs [\%] & mixed pairs [\%]\\
\hline 
\hline
$> 0.50$ & 94.553 & 0.013 & 0.005 & 5.429\\
\hline
$> 0.55$ & 95.042 & 0.010 & 0.004 & 4.944\\
\hline
$> 0.60$ & 95.530 & 0.008 & 0.003 & 4.459\\
\hline
$> 0.65$ & 95.979 & 0.006 & 0.002 & 4.012\\
\hline
$> 0.70$ & 96.455 & 0.004 & 0.001 & 3.538\\
\hline
$> 0.75$ & 96.977 & 0.003 & $<$ 0.001 & 3.019\\
\hline
$> 0.80$ & 97.497 & 0.002 & $<$ 0.001 & 2.501\\
\hline 
\end{tabular}
\label{tab:PurityPairs3}
\end{table}

\newpage

\subsection{Optimization of the IP cut}
\label{sec:PIDCalibration}

In order to keep as much as possible low-$p$ tracks the requirement on the impact parameter has to be optimized, being related to the quality of assignment of the track to the proper PV. In order to estimate the optimal value of the limit on IP, the distribution for MC true pions (identified by MC ID) recognized to originate from the reconstructed PV corresponding to MC PV of their origin has been compared with the distribution of true pions misassigned to the reconstructed PV corresponding the MC PV they are originating from. As it may be seen in Fig.~\ref{fig:IPOpt}, the IP distribution of misassigned tracks seems to be flat in a given range. The selection on IP cut should be optimized to reject the region where the abundance of such tracks becomes significant with respect to those properly assigned. A loose limit on IP has been chosen, i.e. IP~$<$~0.4~mm. The influence of different cuts on IP on the correlation function for data is shown in Fig.~\ref{fig:IPMC} for MC and in Fig.~\ref{fig:IPData} for data, in three different bins of VELO track multiplicity per $pp$ collision. As it can be seen, different requirements on IP make no significant difference in the behavior of the correlation function (see also Sect.~\ref{sec:Stability}).


\subsection{Final selection}
\label{sec:FinalSelection}

For the final selection the requirement on the $probNNpi$ has been chosen to be $> 0.65$, while for the IP, a threshold of 0.4~mm has been applied. Additional vetoes on kaon and proton identification $probNNk < 0.5$ and $probNNp < 0.5$ have been imposed, respectively. The fiducial volume of the measurement has been set in $\eta$ range covered by LHCb, i.e. $2 < \eta < 5$. All the final selection cuts are summarized in Table~\ref{tab:FinalSelection}. Since in the very low $Q$ region ($0.0-0.05$)~GeV the separation in momentum between two particles is very poor and the discrepancy in data and MC track pair reconstruction tends to increase for $Q \rightarrow 0$, the final fits have been performed in the $Q \in (0.05-2.0)$~GeV region. As it has been checked for the MC sample, in the first bin of the distributions of correlation function e.g. in Fig.~\ref{fig:PurityMC} or Fig.~\ref{fig:IPMC}, the fraction of pairs containing a ghost is about $\sim$25\% in all bins of VELO track multiplicity, while the fraction of pairs containing a clone varies from $\sim$8\% for the lowest multiplicity bin up to $\sim$15\% for the highest multiplicity bin. The upper value of the $Q = 2.0$~GeV has been chosen for the final fits, as there is a proper simulation of the long-range correlations in MC as compared to data up to $Q = 2$~GeV (double ratio is approximately flat and close to 1 in this region, see Fig.~\ref{fig:DR_pions}).

Correlation functions for pion pairs from data sample in three bins of VELO track multiplicity per $pp$ collision are shown in Fig.~\ref{fig:CF_RD_pions}. The observed enhancement in the low-$Q$ region corresponds to the expected Bose-Einstein correlations, while for the MC with the BEC effect switched-off, there is no increase observed (see Fig.~\ref{fig:CF_MC_pions}). A little increase in the correlation function distributions for both data and MC is visible for higher $Q$ values. This is related to the effect originating from the long-range correlations, and it seems to be properly simulated in MC in the $Q < 2.0$ GeV, as the constructed double ratio is in good approximation flat and close to 1 in this region (see Fig.~\ref{fig:DR_pions}). They are subtracted when the double ratio is constructed.

Since the BEC effect is expected in the low-$Q$ region, it may be affected by the same-sign clones, as the cloned particle pairs may be peaked at $Q$ close to zero (the detailed study of clones is described in Sect.~\ref{sec:Clones}). Therefore, the additional cut on the differences of two particles' slopes $\Delta t_{x}$ and $\Delta t_{y}$ has been applied, i.e. the pair is removed from the analysis if $|\Delta t_{x}| < 0.3$~mrad and $|\Delta t_{y}| < 0.3$~mrad. The choice of the cut value is motivated by the distributions of $\Delta t_{x}$ and $\Delta t_{y}$ after preselection and after final selection (without cuts on $\Delta t_{x}$ and $\Delta t_{y}$) in Figs.~\ref{fig:tpx},~\ref{fig:tpy},~\ref{fig:tpx51},~\ref{fig:tpy51} from Sect.~\ref{sec:Clones}.

\begin{table}[hbt]
\caption{Final selection cuts.}
\begin{tabular}{|c|c|}
\hline
 & Cut\\
\hline
\hline
track $\eta$ & $2 < \eta < 5$ \\
\hline
track $\chi^{2}$ & $<$~2.0\\
\hline
track momentum & $>$~2.0~GeV\\
\hline
track $p_{T}$ & $>$~0.1~GeV\\
\hline
track IP & $<$~0.4~mm\\
\hline
ProbNNpi & $>$~0.65\\
\hline
ProbNN(kaon,proton) & $<$~0.50\\
\hline
ProbNN(ghost) & $<$~0.25\\
\hline
$|\Delta t_{x}|$, $|\Delta t_{y}|$ & $|\Delta t_{x}| > $~0.3~mrad OR $|\Delta t_{y}| > $~0.3~mrad \\
\hline
\end{tabular}
\label{tab:FinalSelection}
\end{table}


%FIGURES

\newpage

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.35]{Figures/PIDcalib_pion_pionNN.eps}
\includegraphics[scale=0.35]{Figures/PIDCalib_pion_kNN.eps}
\includegraphics[scale=0.35]{Figures/PIDCalib_pion_protonNN.eps}
\caption[]
{The distributions of (top left) $probNNpi$, (top right) $probNNk$ and (bottom) $probNNp$ variables for the MC pion sample before and after PID calibration.}
\label{fig:PIDCalib}
\end{center}
\end{figure}

\newpage

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.35]{Figures/RD_1.eps}
\includegraphics[scale=0.35]{Figures/RD_2.eps}
\includegraphics[scale=0.35]{Figures/RD_3.eps}
\caption[]
{Correlation function for pion pairs from data after preselection with event-mixed reference sample in three different bins of VELO track multiplicity per $pp$ collision: (top left) $<$ 10, (top right) 11-20, (bottom) 21-60. Different cuts on $probNNpi$ have been applied with additional vetoes on kaons and protons ($probNNk < 0.5$ and $probNNp < 0.5$).}
\label{fig:PurityData}
\end{center}
\end{figure}

\newpage

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.35]{Figures/MC_1.eps}
\includegraphics[scale=0.35]{Figures/MC_2.eps}
\includegraphics[scale=0.35]{Figures/MC_3.eps}
\caption[]
{Correlation function for pion pairs from MC sample after preselection with event-mixed reference sample in three different bins of VELO track multiplicity per $pp$ collision: (top left) $<$ 10, (top right) 11-20, (bottom) 21-60. Different cuts on $probNNpi$ have been applied with additional vetoes on kaons and protons ($probNNk < 0.5$ and $probNNp < 0.5$).}
\label{fig:PurityMC}
\end{center}
\end{figure}

\newpage

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.3]{Figures/IPMCPions_lin.eps}
\includegraphics[scale=0.3]{Figures/IPMCPions_log.eps}
\caption[]
{Impact parameter distributions for true pions from MC sample in (left) linear and (right) logarithmic scale. Black dots correspond to the MC true pions properly assigned to the PV corresponding to MC PV of their origin, while the red ones indicate the MC true pions misassigned to the MC PV of their origin.}
\label{fig:IPOpt}
\end{center}
\end{figure}

\newpage

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.35]{Figures/MCIP_1.eps}
\includegraphics[scale=0.35]{Figures/MCIP_2.eps}
\includegraphics[scale=0.35]{Figures/MCIP_3.eps}
\caption[]
{Correlation function for pion pairs from MC after preselection with event-mixed reference sample in three different bins of VELO track multiplicity per $pp$ collision: (top left) $<$ 10, (top right) 11-20, (bottom) 21-60. Different values of the requirement on IP have been applied, i.e. IP $<$ 0.30 mm (blue), IP $<$ 0.35 mm (red) and IP $<$ 0.40 mm (green). Pions with $probNNpi > 0.65$ and additional vetoes on kaons and protons ($probNNk < 0.5$ and $probNNp < 0.5$) have been shown.}
\label{fig:IPMC}
\end{center}
\end{figure}

\newpage

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.35]{Figures/RDIP_1.eps}
\includegraphics[scale=0.35]{Figures/RDIP_2.eps}
\includegraphics[scale=0.35]{Figures/RDIP_3.eps}
\caption[]
{Correlation function for pion pairs from data after preselection with event-mixed reference sample in three different bins of VELO track multiplicity per $pp$ collision: (top left) $<$ 10, (top right) 11-20, (bottom) 21-60. Different values of the requirement on IP have been applied i.e. IP $<$ 0.30 mm (blue), IP $<$ 0.35 mm (red) and IP $<$ 0.40 mm (green). Pions with $probNNpi > 0.65$ and additional vetoes on kaons and protons ($probNNk < 0.5$ and $probNNp < 0.5$) have been shown.}
\label{fig:IPData}
\end{center}
\end{figure}

\newpage

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.35]{Figures/corrFunct_RD_1.eps}
\includegraphics[scale=0.35]{Figures/corrFunct_RD_2.eps}
\includegraphics[scale=0.35]{Figures/corrFunct_RD_3.eps}
\caption[]
{Correlation function for pion pairs from data after final selection described in the text with event-mixed reference sample in three different bins of VELO track multiplicity per $pp$ collision: (top left) $<$ 10, (top right) 11-20, (bottom) 21-60.}
\label{fig:CF_RD_pions}
\end{center}
\end{figure}

\newpage

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.35]{Figures/corrFunct_MC_1.eps}
\includegraphics[scale=0.35]{Figures/corrFunct_MC_2.eps}
\includegraphics[scale=0.35]{Figures/corrFunct_MC_3.eps}
\caption[]
{Correlation function for pion pairs from MC sample after final selection described in the text with event-mixed reference sample in three different bins of VELO track multiplicity per $pp$ collision: (top left) $<$ 10, (top right) 11-20, (bottom) 21-60.}
\label{fig:CF_MC_pions}
\end{center}
\end{figure}
