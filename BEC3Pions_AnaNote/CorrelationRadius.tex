
\section{Determination of the correlation radii}
\label{sec:CorrelationRarius}

The distributions of the double ratio of correlation functions in data and MC for like-sign $\pi\pi$ pairs, defined by assuming the event-mixed reference sample, have been fitted in three different bins of VELO track multiplicity per $pp$ collision using the Lorentzian parametrization (equation~\ref{eqt:CorrFunctExp}) in order to estimate the width of the enhancement observed in the low-$Q$ region. The binned maximum likelihood fit has been used. The fits to the double ratio together with pull distributions are presented in Fig.~\ref{fig:Fits_DR_pions}. The fitted range of the $Q$ variable amounted to $0.05 < Q < 2.0$~GeV, as described in Sect.~\ref{sec:FinalSelection}. The results of fits to the double ratios for pions in three different bins of VELO track multiplicity per $pp$ collision and corresponding activity classes are collected in Table~\ref{tab:Fits_DR_pions_abs} quoting absolute error values and in Table~\ref{tab:Fits_DR_pions} quoting fractional errors.

\begin{table}[hbt]
\caption{Results of fits to the double ratio $DR(Q)$ for like-sign $\pi\pi$ pairs with the event-mixed reference sample in three different bins of VELO track multiplicity per $pp$ collision $N_{ch}$ and corresponding activity classes, using parametrization~(\ref{eqt:CorrFunctExp}). Only absolute values of statistical errors are listed.}
\begin{tabular}{|c|c|c|c|c|c|c|c|}
\hline
$N_{ch}$ & Activity class & $R$ [fm] & $\lambda$ & $N$ & $\delta$ [GeV$^{-1}$] & $\chi^{2}$/ndf\\
\hline
\hline
$< 10$ & (52-100)\%  & 1.01 $\pm$ 0.01 & 0.72 $\pm$ 0.01 & 0.89 $\pm$  ($<$0.01) & 0.09 $\pm$ ($<$0.01) & 591/386\\
\hline
$11 - 20$ & (15-52)\% & 1.48 $\pm$ 0.02 & 0.63 $\pm$ 0.01 & 0.94 $\pm$  ($<$0.01) & 0.05 $\pm$ ($<$0.01) & 623/386\\
\hline
$21 - 60$ & (0-15)\% & 1.80 $\pm$ 0.03 & 0.57 $\pm$ 0.01 & 0.97 $\pm$  ($<$0.01) & 0.03 $\pm$ ($<$0.01) & 621/386\\
\hline
\end{tabular}
\label{tab:Fits_DR_pions_abs}
\end{table}


\begin{table}[hbt]
\caption{Results of fits to the double ratio $DR(Q)$ for like-sign $\pi\pi$ pairs with the event-mixed reference sample in three different bins of VELO track multiplicity per $pp$ collision $N_{ch}$ and corresponding activity classes, using parametrization~(\ref{eqt:CorrFunctExp}). Only fractional statistical errors are listed.}
\begin{tabular}{|c|c|c|c|c|c|c|c|} 
\hline 
$N_{ch}$ & Activity class & $R$ [fm] & $\lambda$ & $N$ & $\delta$ [GeV$^{-1}$] & $\chi^{2}$/ndf\\
\hline 
\hline
$< 10$ & (52-100)\%  & 1.01 $\pm$ 1.3\% & 0.72 $\pm$ 0.8\% & 0.89 $\pm$  0.3\% & 0.09 $\pm$ 2.6\% & 591/386\\
\hline
$11 - 20$ & (15-52)\% & 1.48 $\pm$ 1.2\% & 0.63 $\pm$ 1.3\% & 0.94 $\pm$  0.1\% & 0.05 $\pm$ 2.0\% & 623/386\\
\hline
$21 - 60$ & (0-15)\% & 1.80 $\pm$ 1.6\% & 0.57 $\pm$ 2.0\% & 0.97 $\pm$  0.1\% & 0.03 $\pm$ 2.9\% & 621/386\\
\hline
\end{tabular}
\label{tab:Fits_DR_pions}
\end{table}

As it may be seen in Table~\ref{tab:Fits_DR_pions} the value of the correlation radius $R$ increases with the VELO track multiplicity, while the chaoticity parameter $\lambda$ is decreasing. The pull distributions in Fig.~\ref{fig:Fits_DR_pions} show a good quality of the fit, since almost all points stay within the 3$\sigma$, indicating that there is no significant disagreement between the model and fitted $DR$.

The correlation matrix for the fit parameters in the bin of VELO track multiplicity (11-21) is given in Table~\ref{tab:corrMatrix}. 
\begin{table}[hbt]
\caption{The correlation matrix for the fit parameters $N$, $\lambda$, $R$ and $\delta$ in the bin of VELO track multiplicity (11-21).}
\begin{tabular}{|c|c|c|c|c|c|c|c|} 
\hline 
 & $N$ & $\lambda$ & $R$ & $\delta$ \\
\hline 
\hline
$N$  & 1.0 & 0.36 & 0.75 & -0.96\\
\hline
$\lambda$ & 0.36 & 1.0 & 0.82 & -0.29\\
\hline
$R$ & 0.75 & 0.82 & 1.0 & -0.67\\
\hline
$\delta$ & -0.96 & -0.29 & -0.67 & 1.0\\
\hline
\end{tabular}
\label{tab:corrMatrix}
\end{table}


\subsection{Unfolding of $N_{ch}$}
\label{sec:Unfolding}

Although the best way to compare results among various experiments is to introduce activity classes, the unfolding procedure has been performed to relate the reconstructed PV VELO track multiplicity $N_{ch}$ to the one predicted by the MC generator, i.e. PYTHIA 8 with LHCb tuning. It has to be noted that this unfolding procedure is model dependent, aiming at providing a qualitative picture of the dependence on the total VELO track multiplicity of PV in the LHCb acceptance. A Bayesian unfolding technique~\cite{Bayes} within the ROOT Unfolding Framework (RooUnfold)~\cite{RooUnfold} has been used, based on the construction of an unfolding matrix convolved with a normalized Gaussian distribution to account for the experimental resolution on the number of selected VELO tracks (other parametrizations have been also tried, but since they do not provide any sizable asymmetry in errors, the simplest Gaussian parametrization has been finally used). It relates two PV multiplicities in LHCb acceptance ($2 < \eta < 5$) determined on simulation:  $N_{ch}^{MC}$ obtained in the same way as $N_{ch}$ for data, and generated $N_{ch}^{gen}$ which denotes the number of generated charged particles originating directly from the MC PV (true prompt particles). The unfolding procedure converges after the third iteration. The unfolded multiplicity distribution is compared with the generated one in Fig.~\ref{fig:Unfold}, together with a covariance matrix. The difference observed for low multiplicities does not affect the determination of the bins for unfolded charged particle multiplicity. In the case of data there is an irreducible contamination of the not prompt particles misassigned as the prompt ones. It has been checked in the simulation that the total rate of such misassigned particles is 0.03\%, where the dominating source is the photon conversion ($\sim$67\%), and the rest comes mostly from $V^{0}$s as well as $c$- and $b$-hadrons. This effect has been found to be negligible for the final unfolded charged particle multiplicity bin splitting. The relation between folded and unfolded bin ranges is summarised in Table~\ref{tab:Unfolding}. The observed relation agrees well with the dependencies estimated in the LHCb analysis of the charged particle multiplicity~\cite{ChMult}.

\begin{table}[hbt]
\caption{The relation between folded and unfolded bin ranges.}
\begin{tabular}{|c|c|c|c|c|} 
\hline
\hline
$N_{ch}$ before unfolding & $<$ 10 & 11 - 20 & 21 - 60\\
\hline
$N_{ch}$ after unfolding & $<$ (18$\pm$0.15) & (19$\pm$0.15) - (35$\pm$0.39) & (36$\pm$0.40) - (96$\pm$1.18)\\
\hline
\end{tabular}
\label{tab:Unfolding}
\end{table}

\newpage

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.35]{Figures/DRFit_pion_1.eps}
\includegraphics[scale=0.35]{Figures/DRFit_pion_2.eps}
\includegraphics[scale=0.35]{Figures/DRFit_pion_3.eps}
\caption{Results of the fit to the double ratio for like-sign pion pairs with event-mixed reference sample and the Coulomb effect subtracted in three different bins of VELO track multiplicity per $pp$ collision: (top left) $<$ 10, (top right) 11-20, (bottom) 21-60. Red solid line denotes the fit result using parametrization described in the text. Corresponding pull distributions are shown on the bottom. R parameter is expressed in GeV$^{-1}$, and it is recalculated to [fm] using factor 0.197 (see Table~\ref{tab:Fits_DR_pions}).}
\label{fig:Fits_DR_pions}
\end{center}
\end{figure}


\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.45]{Figures/unfoldedNch.eps}
\includegraphics[scale=0.45]{Figures/UnfoldCovMatrix2.eps}
\caption{(Top) Measured (blue histogram), generated (green histogram) and unfolded (red figures) multiplicity distribution using the unfolding technique described in the text. (Bottom) Unfolded covariance matrix.}
\label{fig:Unfold}
\end{center}
\end{figure}

\clearpage

\newpage
