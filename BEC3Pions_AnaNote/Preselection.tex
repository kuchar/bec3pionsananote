
\section{Event selection}
\label{sec:EvtSelection}

The analysis is based on the events with pairs of positively identified pions, which flag the possible presence of the Bose-Einstein correlations.


\subsection{Data and Monte Carlo samples}
\label{sec:Samples}

In the present note the data collected in 2011 at the center-of-mass energy of $\sqrt{s}$ = 7 TeV has been analysed. The analysis uses a sample of about 40~M minimum bias stream Stripping20r1 Reco14 data available for 2011. See Sect.~\ref{B} (Appendix B) for more details.

The proton-proton collisions are simulated using PYTHIA~8~\cite{Pythia} as the physics event generator, where the Bose-Einstein and Fermi-Dirac correlations are switched-off. A sample of $\sim$20~M simulated minimum bias events, reconstructed with Sim08 Reco14 configuration, has been used. To study the effect related to the MC generation the locally produced samples of $\sim$10~M minimum bias events each using PYTHIA~6.4 with Perugia0 tuning~\cite{Perugia0} as well as HERWIG++~2.7.1~\cite{Herwig} generators with BEC effect switched-off have been employed, with the generation and reconstruction corresponding to the MC2011 production. Details on the simulated samples may be found in Sect.~\ref{B} (Appendix B).

In the present analysis the NoBias PV sample has been used. Since in the case of MC all the events are NoBias, the PVs from the same (multi-PV) events may be treated as fully uncorrelated single PVs. In the case of data the construction of the NoBias PV sample is the following. If the event is selected by NoBias stripping line, all PVs are accepted as in the case of MC. However, to increase the statistics for data, if the event is not selected by NoBias stripping line, the PVs containing TOS track(s) which fired minimum bias trigger have been selected. Then one of such PVs has been randomly removed and the rest has been analysed as a NoBias PV sample.


\subsection{Preselection}
\label{sec:Preselection}

The preselection requires at least one pair of loosely identified pions, kaons or protons originating from the same primary vertex. An individual particle is assigned to the PV according to the minimum value of the $\chi^{2}$ of its impact parameter (IP). Only ’long’ tracks have been considered. The full list of requirements is given in Table~\ref{tab:Preselection}. The contamination from muons has been reduced by requiring that the particle's {\it isMuon} flag is false.

\begin{table}[hbt]
\caption{Requirements on the tracks to form a pair of pions. The particle identification probability {\it ProbNN} is a MC-trained neural network output that quantifies the probability for the particle to be of a certain kind.}
$$\begin{tabular}{|c|c|} 
\hline 
 & Cut\\
\hline 
\hline
track $\chi^{2}$ & $<$~2.6\\
\hline
track momentum & $>$~2.0~GeV\\
\hline
track $p_{T}$ & $>$~0.1~GeV\\
\hline
track IP & $<$~0.4 mm\\
\hline
ProbNN(ghost) & $<$~0.5\\
\hline 
\end{tabular}$$
\label{tab:Preselection}
\end{table}

The data sample which passed the above-mentioned criteria corresponds to $\sim$0.5~M events. The distributions of variables used in the preselection procedure are shown in Fig.\ref{fig:TrkChi2IP}-\ref{fig:PNNghostNN}. The arrows indicate the regions accepted for futher analysis.

The material description is not perfectly modeled in the simulation (see Fig~\ref{fig:PhiIPCorr}). Thus, in order to avoid any second order distortions in the double ratio related to differences between data and simulation, only the cut on IP is used to remove tracks not originating directly from primary interaction.

After preselection requirements listed in Table~\ref{tab:Preselection} the total number of $\sim$2.5~$\times$~10$^{8}$ of $\pi \pi$ pairs remained for further analysis.


\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.30]{Figures/trackChi2.eps}
\includegraphics[scale=0.30]{Figures/IP.eps}
\caption[]
{(left) $\chi^2$ of the reconstructed track and (right) track impact parameter for 2011 NoBias PV data sample. Red arrows indicate the cuts applied.}
\label{fig:TrkChi2IP}
\end{center}
\end{figure}

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.30]{Figures/p.eps}
\includegraphics[scale=0.30]{Figures/pT.eps}
\caption[]
{(left) Momentum of the track and (right) track transverse momentum for 2011 NoBias PV data sample. Red arrows indicate the cuts applied.}
\label{fig:TrkPPt}
\end{center}
\end{figure}

\newpage

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.30]{Figures/pionNN.eps}
\includegraphics[scale=0.30]{Figures/kaonNN.eps}
\caption[]
{Particle identification probability {\it ProbNN} for (left) pion and (right) kaon hypothesis for 2011 NoBias PV data sample. Red arrows indicate the cuts applied.}
\label{fig:PiNNkNN}
\end{center}
\end{figure}

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.30]{Figures/protonNN.eps}
\includegraphics[scale=0.30]{Figures/ghostNN.eps}
\caption[]
{Particle identification probability {\it ProbNN} for (left) proton and (right) 
 ghost hypothesis for 2011 NoBias PV data sample. Red arrows indicate the cuts applied.}
\label{fig:PNNghostNN}
\end{center}
\end{figure}

\newpage

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.30]{Figures/IPChi2_1.eps}
\includegraphics[scale=0.30]{Figures/IPChi2_2.eps}
\caption[]
{IP$\chi^{2}$ vs azimuthal angle for (left) all particles from 2011 NoBias PV data sample and (right) true pions from MC2011 minimum bias events.}
\label{fig:PhiIPCorr}
\end{center}
\end{figure}

